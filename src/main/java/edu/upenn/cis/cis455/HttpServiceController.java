package edu.upenn.cis.cis455;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.TimeZone;

import edu.upenn.cis.cis455.exceptions.HaltException;
import edu.upenn.cis.cis455.handlers.Filter;
import edu.upenn.cis.cis455.handlers.Route;
import edu.upenn.cis.cis455.m1.server.HttpServer;
import edu.upenn.cis.cis455.m1.server.implementations.RequestImplementation;
import edu.upenn.cis.cis455.m1.server.interfaces.WebService;
import edu.upenn.cis.cis455.m2.server.interfaces.Session;

public class HttpServiceController extends WebService {

	int port;
	String directory;
	Path dir;
	HttpServer basicServer;
	int threadPoolSize = 30;
	int requestQueueSize = 1000;
	
	 String ipAddress=null;
	
	private static HttpServiceController singleton = new HttpServiceController( );
	static RouteMap routeMap=new RouteMap();//create new static map
	 FilterStructure beforeStructure=new FilterStructure();
	 FilterStructure afterStructure =new FilterStructure();

	
	
	HashMap <String, Session> SessionMap=new HashMap<String, Session>();	

	   /* A private Constructor prevents any other
	    * class from instantiating.
	    */
	   private HttpServiceController() { }

	   /* Static 'instance' method */
	   public static HttpServiceController getInstance( ) {
	      return singleton;
	   }


	
	
	
	@Override
	public void start() throws IOException {

		basicServer=new HttpServer(threadPoolSize,requestQueueSize,directory);
		try {
			
			
			basicServer.initializeHttpServer(port,this.ipAddress);
			
			
			
			
		} catch (InterruptedException e) {
			//e.printStackTrace();
		}

	}

	@Override
	public void stop() {
		// TODO Auto-generated method stub
		
		
	}

	@Override
	public void staticFileLocation(String directory) {
		// TODO Auto-generated method stub
		this.directory=directory;
		this.dir=Paths.get(directory);
		
	}

	@Override
	public void get(String path, Route route) {
		// TODO Auto-generated method stub

		
		routeMap.put("GET",path, route);
	}

	@Override
	public void ipAddress(String ipAddress) {
		// TODO Auto-generated method stub
		this.ipAddress=ipAddress;
	}

	@Override
	public void port(int port) {
		// TODO Auto-generated method stub
		this.port=port;

	}
	
	public String ipAddress() {
		// TODO Auto-generated method stub
		return this.ipAddress;
	}


	public String port() {
		// TODO Auto-generated method stub
		return Integer.toString(this.port);

	}

	@Override
	public void threadPool(int threads) {
		// TODO Auto-generated method stub
		this.threadPoolSize=threads;
		

	}
	public void awaitInitialization() throws IOException {
		logger.info("Initializing server");
//		forbiddenPathFilter();
//		wrongProtocolFilter();
//		checkDateFormatFilter();
//		ifModified();
//		IfUnmodified();
//		getDefaultHandler();
//		getAllfilesInRoot();
		
		start();
	}
	private void getAllfilesInRoot(){
		
		logger.info("registering getAllfilesInRoot");
		get("/*", (request,response)->{  
			InputStream fs=null;
			String root=WebServiceController.directory();
			String fileString=null;
					
			File file= new File(root+( request).pathInfo());
			logger.debug("Inside handle method of get all files");
			logger.debug(root+( request).pathInfo());
			try {
				fs= new FileInputStream(root+( request).pathInfo());
			} catch (FileNotFoundException e) {
				String body="<html><head><title>Fail!!</title></head><body><h1>HTTP/1.1</h1><p>file not found</p></body></html>";
				HaltException x =new HaltException(404,body);
				throw x;



			}
			byte[] b= new byte[(int)file.length()];


			int c=0;
			try {
				c=fs.read((byte[])b);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			response.bodyRaw(b);


			response.contentLength(c);
			response.status(200);
			response.type(request.contentType());
			fs.close();
			return null;
			
			
			
		});
		
	}
	private void getDefaultHandler(){
		logger.info("registering default route handler");

		get("/",(request, response)->{
			int c;
			File file=null;
			String root=WebServiceController.directory();
			InputStream fs=null;
			String fileString=null;

			BufferedReader buf=null;
			RequestImplementation req=(RequestImplementation)request;
			StringBuilder sb=null;
			try {
	
	
					file= new File(root+( request).pathInfo()+"index.html");
					fs= new FileInputStream(file);
					buf = new BufferedReader(new InputStreamReader(fs));
					sb = new StringBuilder();
	
			} 
			catch (FileNotFoundException e) {
					
					String body="<html><head><title>Fail!!</title></head><body><h1>HTTP/1.1</h1><p>file not found</p></body></html>";
					throw new HaltException(404,body);
	
			}
				String line=null;
				try {
						line = buf.readLine();
						while(line !=null){
							sb.append(line).append("\n");
							line = buf.readLine();
		
		
						}
					
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				fileString=sb.toString();
				response.contentLength((int)file.length());
				//String body="<html><head><title>Response</title></head><body><h1>Response</h1><p>test</p></body></html>";
				response.body(fileString);
				logger.debug(fileString);
				response.bodyRaw(fileString.getBytes());
				response.status(200);
				response.type("text/html");
				try {
					fs.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	return null;
			
			
			
		});
	}
	
	private void checkDateFormatFilter() {
		
		logger.info("registering checkDateFormatFilter");
		before((request,response)->{		
			String date1=null;
			if(request.headers("date")!=null){
				//date in GMT
				date1 =request.headers("date");

			}
			else{return ; }
				logger.debug("******************<Checking Date>*****************");
				
				SimpleDateFormat sdf = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss z");
				sdf.setTimeZone(TimeZone.getTimeZone("G"));
	
	
				Date parsedDate=null;
				try {
					parsedDate= sdf.parse(date1);
					//logger.debug(parsedDate);
					
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				//logger.debug(date1);
	
				logger.debug(sdf.format(parsedDate));
							logger.debug(date1);
					logger.debug(sdf.format(parsedDate));
				if(date1.equals(sdf.format(parsedDate))){
					logger.debug(date1);
					logger.debug(sdf.format(parsedDate));
					return;
				}
				else{
					HaltException e= new HaltException(400,null,"Date format wrong");
					throw e;
				}
		
		
		
			}
		);
		
	}
private void IfUnmodified(){
	logger.info("registering IfUnmodified");
	before((request, response)->{
		logger.debug("request.headers(\"If-Unmodified-Since\"):"+request.headers("if-unmodified-since"));
		if(request.headers("if-unmodified-since")!=null){
			logger.debug("*********************<If-Unmodified-Since>*********************");
			
			SimpleDateFormat sdf = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss z");
			sdf.setTimeZone(TimeZone.getTimeZone("G"));
			String modifiedDateString=request.headers("if-unmodified-since");
			
			
			Date modifiedDate=null;
			try {
				modifiedDate= sdf.parse(modifiedDateString);
				//logger.debug(parsedDate);
				
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			//logger.debug(date1);

			logger.debug(sdf.format(modifiedDate));
						logger.debug(modifiedDateString);
						
				logger.debug(sdf.format(modifiedDate));
				//check whether modifiedDateString is in proper format
			if(modifiedDateString.equals(sdf.format(modifiedDate))){ 
					
					Date fileDate=null;
					Date systemDate=new Date();
					String requestPathString= new String(WebServiceController.directory());
					
					requestPathString+=request.pathInfo();//request				
					
					File file = new File(requestPathString);
					logger.debug("After Format : " + sdf.format(file.lastModified()));
					try {
						fileDate= sdf.parse((sdf.format(file.lastModified())));
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					if(fileDate.compareTo(modifiedDate)>0){
						HaltException e= new HaltException(412, null, "Precondition Failed");
						throw e;
					}
					else if(modifiedDate.compareTo(systemDate)>0){
						
						
						//sent date greater than system date. modified in future LOL
						HaltException e= new HaltException(400,null,"Bad request");
						e.getLocalizedMessage();
						throw e;
						
					}
			
			}
			else{
				HaltException e= new HaltException(400,null,"If unmodified date is in wrong format");
				throw e;
			}
		}
		
		
		
		
		
		
		
	});
	
	
	
	
	
}

	private void wrongProtocolFilter() {
		// TODO Auto-generated method stub
		logger.info("registering wrongProtocolFilter");

		before((request,response)-> {
			String protocolVersion= new String();
			protocolVersion=request.headers("protocolVersion");
			if(protocolVersion==null){
				protocolVersion="HTTP/1.1";
			}
			logger.debug(protocolVersion);
			 if(protocolVersion.equals("HTTP/1.1")!=true){
				 logger.debug("*********************<protocolVersion.equals(\"HTTP/1.1\")!=true>*********************");
				// HTTP/1.0 throw code 505 version not supported
				HaltException e= new HaltException(505,null, "Wrong Protocol");
				throw e;	
			}
		});
		
	}


	private void forbiddenPathFilter() {
		logger.info("registering forbiddenPathFilter");

		// TODO Auto-generated method stub
		before((request,response)-> {
			String requestPathString= new String(WebServiceController.directory());
			
			requestPathString+=request.pathInfo();//request
			
			Path requestPath= Paths.get(requestPathString);
			
			requestPath=requestPath.toAbsolutePath().normalize();
			logger.debug(requestPath);
			String path=requestPath.toString();
			String root=new String(directory);			
			if(path.startsWith(root.toString())!=true){
		
				logger.debug("*********************<Forbidden>*********************");
				HaltException e= new HaltException(403,null,"Forbidden");
				throw e;
			
			}
		});
	
		
	}
	private void ifModified(){
		
		logger.info("registering ifModified");

		before((request,response)-> {
			logger.debug("request.headers(\"If-Modified-Since\"):"+request.headers("if-modified-since"));
			
			if(request.headers("if-modified-since")!=null){
				
				logger.debug("*********************<If-Modified-Since>*********************");
				
				
				String modifiedDateString=request.headers("if-modified-since");
				
				
				logger.debug("request.headers(\"modified\"):"+request.headers("if-modified-since"));			
				
				logger.debug("******************<Checking Date>*****************");
				
				SimpleDateFormat sdf = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss z");
				sdf.setTimeZone(TimeZone.getTimeZone("G"));
				
	
				Date modifiedDate=null;
				try {
					modifiedDate= sdf.parse(modifiedDateString);
					//logger.debug(parsedDate);
					
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				//logger.debug(date1);
	
				logger.debug(sdf.format(modifiedDate));
							logger.debug(modifiedDateString);
							
					logger.debug(sdf.format(modifiedDate));
					//check whether modifiedDateString is in proper format
				if(modifiedDateString.equals(sdf.format(modifiedDate))){ 
					
					
					
					logger.debug(modifiedDateString);
					logger.debug(sdf.format(modifiedDate));
					

					Date fileDate=null;
					Date systemDate=new Date();
					String requestPathString= new String(WebServiceController.directory());
					
					requestPathString+=request.pathInfo();//request				
					File file = new File(requestPathString);
					
					logger.debug("After Format : " + sdf.format(file.lastModified()));
					try {
						fileDate= sdf.parse((sdf.format(file.lastModified())));
					} catch (ParseException e) {
						
						e.printStackTrace();
					}
					
					if(fileDate.compareTo(modifiedDate)<=0){
						HaltException e= new HaltException(304, null, "Not Modified");
						throw e;
					}
					else if(modifiedDate.compareTo(systemDate)>0){
						
						
						//sent date greater than system date. modified in future LOL
						HaltException e= new HaltException(400,null,"Bad request");
						e.getLocalizedMessage();
						throw e;
						
					}
					
					
					
					return;
					
					
				
				}
				else{
					HaltException e= new HaltException(400,null,"Date format wrong");
					throw e;
				}
		
				
				
				
				
			}
			else{
				return;
			}
			
			
			
			
		});
		
		
	}
	
	
	
	

	/**
	 * Triggers a HaltException that terminates the request
	 */
	
	
	public HaltException halt() {
		throw new HaltException();
	}


	/**
	 * Triggers a HaltException that terminates the request
	 */
	public HaltException halt(int statusCode) {
		throw new HaltException(statusCode);
	}

	/**
	 * Triggers a HaltException that terminates the request
	 */
	public HaltException halt(String body) {
		throw new HaltException(body);
	}

	/**
	 * Triggers a HaltException that terminates the request
	 */
	public HaltException halt(int statusCode, String body) {
		throw new HaltException(statusCode, body);
	}

	@Override
	public void post(String path, Route route) {
		// TODO Auto-generated method stub
		routeMap.put("POST",path, route);
	}

	@Override
	public void put(String path, Route route) {
		// TODO Auto-generated method stub
		routeMap.put("PUT",path, route);
	}

	@Override
	public void delete(String path, Route route) {
		// TODO Auto-generated method stub
		routeMap.put("DELETE",path, route);
	}

	@Override
	public void head(String path, Route route) {
		// TODO Auto-generated method stub
		routeMap.put("HEAD",path, route);
	}

	@Override
	public void options(String path, Route route) {
		// TODO Auto-generated method stub
		routeMap.put("OPTIONS",path, route);
	}

	@Override
	public void before(Filter filter) {
		// TODO Auto-generated method stub
		
		beforeStructure.put(filter);
	}

	@Override
	public void after(Filter filter) {
		// TODO Auto-generated method stub
		afterStructure.put(filter);
	}

	@Override
	public void before(String path, String acceptType, Filter filter) {
		// TODO Auto-generated method stub
		beforeStructure.put( acceptType,  path,  filter);
	}

	@Override
	public void after(String path, String acceptType, Filter filter) {
		// TODO Auto-generated method stub

		afterStructure.put( acceptType,  path,  filter);
	}


}
