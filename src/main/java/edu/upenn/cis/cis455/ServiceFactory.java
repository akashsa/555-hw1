package edu.upenn.cis.cis455;

import java.net.Socket;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.UUID;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import edu.upenn.cis.cis455.m1.server.interfaces.WebService;
import edu.upenn.cis.cis455.exceptions.HaltException;
import edu.upenn.cis.cis455.m1.server.HttpIoHandler;
import edu.upenn.cis.cis455.m1.server.HttpWorker;
import edu.upenn.cis.cis455.m1.server.implementations.RequestImplementation;
import edu.upenn.cis.cis455.m1.server.implementations.ResponseImplementation;
import edu.upenn.cis.cis455.m1.server.implementations.SessionImplementation;
import edu.upenn.cis.cis455.m1.server.implementations.HttpRequestHandlerImplementation;
import edu.upenn.cis.cis455.m1.server.interfaces.HttpRequestHandler;
import edu.upenn.cis.cis455.m2.server.interfaces.Request;
import edu.upenn.cis.cis455.m2.server.interfaces.Response;
import edu.upenn.cis.cis455.m2.server.interfaces.Session;


public class ServiceFactory {
	final static Logger logger = LogManager.getLogger(ServiceFactory.class);
	//org.apache.logging.log4j.core.config.Configurator.setLevel("edu.upenn.cis.cis455.ServiceFactory", Level.);
    /**
     * Get the HTTP server associated with port 8080
     */
	static String requestBody=null;
	
    public static WebService getServerInstance() {
        return HttpServiceController.getInstance();
    }
    
    /**
     * Create an HTTP request given an incoming socket
     */
    public static Request createRequest(Socket socket,
                         String uri,
                         boolean keepAlive,
                         Map<String, String> headers,
                         Map<String, List<String>> parms) {
    	
    	
    	
    	System.out.println(socket.getRemoteSocketAddress().toString());
    
    	RequestImplementation request=new RequestImplementation(headers,parms,uri,socket.getPort(), socket.getRemoteSocketAddress().toString());
    	return request;
   
    	
    
		
		/*

		http://www.example.com/docs/resource1.html?param1=value1
		 
		The methods will return the following:

		1) pathInfo() - /docs/resource1.html
		2) url() - http://www.example.com/docs/resource1.html?param1=value1
		3) uri() - http://www.example.com/docs/resource1.html
		*/
	
		
        
    }
    
    /**
     * Gets a request handler for files (i.e., static content) or dynamic content
     */
    public static HttpRequestHandler createRequestHandlerInstance(Path serverRoot) {
    	HttpRequestHandlerImplementation httpRequestHandler = new HttpRequestHandlerImplementation(serverRoot);
    	
        return httpRequestHandler;
    }

    /**
     * Gets a new HTTP Response object
     */
    public static Response createResponse() {
    	ResponseImplementation response = new ResponseImplementation();
        return response;
    }

    /**
     * Creates a blank session ID and registers a Session object for the request
     */
    public static String createSession() {
    	String sessionId=UUID.randomUUID().toString();
    	HashMap<String, Session> sessionMap= WebServiceController.getSessionMap();
    	sessionMap.put(sessionId, new SessionImplementation(sessionId));
    	
        return sessionId;
    }
    
    /**
     * Looks up a session by ID and updates / returns it
     */
    public static Session getSession(String id) {
    	long currentTime=ServiceFactory.getCurrentTimeMs();
    	HashMap<String, Session> sessionMap= WebServiceController.getSessionMap();
    	Session s=sessionMap.get(id);
    	if((currentTime-s.lastAccessedTime())>s.maxInactiveInterval()){
    		sessionMap.remove(id);
    		return null;
    	}
    	else{
    		return s;
    	}
    	
    }
    
    
    
    
    public static long getCurrentTimeMs() {
		 Date currentTime= new Date();
		 SimpleDateFormat currentTimesdf= new SimpleDateFormat("EEE, dd MMM, yyyy HH:mm:ss z");
		
		currentTimesdf.setTimeZone(TimeZone.getTimeZone("GMT"));
        return currentTime.getTime();
    }
    public static String getCurrentTimeString() {
		 Date currentTime= new Date();
		 SimpleDateFormat currentTimesdf= new SimpleDateFormat("EEE, dd MMM, yyyy HH:mm:ss z");
		currentTimesdf.setTimeZone(TimeZone.getTimeZone("GMT"));
		
        return currentTimesdf.format(currentTime);
    }
    
    
    
    
    
}