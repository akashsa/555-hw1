package edu.upenn.cis.cis455;
import java.util.*;

import edu.upenn.cis.cis455.handlers.*;

public class RouteMap {
	 public  Map<String, Map<String, Route>>routeMap=null;
 
 
 
	public RouteMap() {		
		routeMap = new HashMap<String, Map<String, Route>>();
		//routeMap = new HashMap<String, Map<String, Route>>();


				// TODO Auto-generated constructor stub
	
		routeMap.put("GET", new HashMap<String, Route>());
		routeMap.put("HEAD", new HashMap<String, Route>());
		routeMap.put("POST", new HashMap<String, Route>());
		routeMap.put("DELETE", new HashMap<String, Route>());
		routeMap.put("OPTIONS", new HashMap<String, Route>());
	}
	public synchronized  void put(String routeMethod, String path, Route route){
		this.routeMap.get(routeMethod).put(path, route);
		
		
	}
	public synchronized  Route get(String routeMethod, String path){
		return this.routeMap.get(routeMethod).get(path);
		
		
	}
	public synchronized  Set<String> keySet(String routeMethod){
		return this.routeMap.get(routeMethod).keySet();
		
		
	}
	

}
