package edu.upenn.cis.cis455;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.file.Path;
import java.nio.file.Paths;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class WebServer {
	
	static final Logger logger = LogManager.getLogger(WebServer.class);
	
    public static void main(String[] args) throws IOException {
       //org.apache.logging.log4j.core.config.Configurator.setLevel("edu.upenn.cis.cis455", Level.DEBUG);
    	Integer port=8080;
    	String root=System.getProperty("user.dir").toString()+"/www";
    	logger.debug(args);
        // TODO: make sure you parse *BOTH* command line arguments properly
    	logger.debug(args.length);
    	switch(args.length){
    	case 1: try{
    			port= Integer.parseInt(args[0]);
    			}
    			catch(Exception e){
    				e.printStackTrace();
    				
    			}
    			root= System.getProperty("user.dir").toString()+"/www";
    			break;
    	case 2:	try{
					port= Integer.parseInt(args[0]);
				}
				catch(Exception e){
					e.printStackTrace();
				
				}
				root= (args[1]);
				break;
    	case 0: break;		
    	default:logger.error("number of arguments should be x<=2"); 
    			System.exit(1); 
    			
    	}
    	System.out.println("starting server");
    	//HttpServiceController webservicecontroller=new HttpServiceController();
    	WebServiceController.ipAddress("localhost");
    	WebServiceController.port(port);
    	WebServiceController.staticFileLocation(root);
    	WebServiceController.awaitInitialization();
    	
    	
    	  System.out.println("Waiting to handle requests!");
    	     	
    }

}
