package edu.upenn.cis.cis455.m1.server.implementations;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.TimeZone;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import edu.upenn.cis.cis455.m2.server.interfaces.Response;

public class ResponseImplementation extends Response {

	static final Logger logger= LogManager.getLogger(ResponseImplementation.class);
	protected int statusCode = 200;
	protected byte[] body;
	protected String contentType = null;//"text/plain";
	public String Header;
	final String CRLF="\r\n" ;
	protected String stringResponse;
	//protected int contentLength=0;
	protected String test=null;
	HashMap <String, HashMap<String, String>> CookieMap= new HashMap <String, HashMap<String, String>>();
	//HashMap<String, String> directives  =new HashMap<String, String>();
	
	
	public ResponseImplementation( ) {
		// TODO Auto-generated constructor stub
	}

	@Override
	public String getHeaders() {
		// TODO Auto-generated method stub

		final Date currentTime= new Date();
		final SimpleDateFormat currentTimesdf= new SimpleDateFormat("EEE, dd MMM, yyyy HH:mm:ss z");
		
		currentTimesdf.setTimeZone(TimeZone.getTimeZone("GMT"));
		Header= "Content-Type:"+type()+CRLF
				+"Content-Length:"+contentLength+CRLF
				+"Date: "+currentTimesdf.format(currentTime)+CRLF;
		//logger.debug(Header);
		logger.debug("Header= "+Header);
		String cookie =new String("");
		
		

		Iterator itr=CookieMap.keySet().iterator();
		
		while(itr.hasNext()){
			String x = new String("Right now");
			
			cookie+="Set-Cookie: ";
			logger.debug("Set-Cookie: "+cookie);
			String Key=(String)itr.next();
			
			HashMap map=CookieMap.get(Key);
			String lol=Key+"="+map.get("value").toString();
			logger.debug("lol="+lol);
			cookie+=lol ;
			
			if(map.get("maxAge")!=null){
				String new1="; "+"maxAge="+map.get("maxAge");
				cookie+=(new1);
				logger.debug(cookie + map.get("maxAge"));
			}
			if(map.get("secured")!=null && map.get("secured").equals("true") ){
				cookie+=("; "+"secured");
				logger.debug(cookie);
			}
			if(map.get("httpOnly")!=null && map.get("httpOnly").equals("true") ){
				cookie+=("; "+"HttpOnly");
				logger.debug(cookie);
			}
			if(map.get("path")!=null){
				cookie+=("; "+"Path="+map.get("path"));
				logger.debug(cookie);
			}
			cookie+=("\r\n");
			System.out.println("(((((("+cookie);
			logger.debug(cookie);
		}
		
		Header+=(cookie+CRLF);
		
		
		
		return Header;
	}


	public int status() {
		return statusCode;
	}

	public void status(int statusCode) {
		this.statusCode = statusCode;
	}

	public String body() {
		try {
			return body == null ? "" : new String(body, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "";
		}
	}

	public byte[] bodyRaw() {
		return body;
	}

	public void bodyRaw(byte[] b) {
		body = b;
	}

	public void body(String body) {
		this.body = body == null ? null : body.getBytes();
	}

	public String type() {
		return contentType;
	}

	public void type(String contentType) {
		this.contentType = contentType;
	}

	public  void responseString(String responseString) {
		this.stringResponse = responseString;
	}
	public  String responseString() {
		return this.stringResponse;
	}
	public  void contentLength(int length) {
		this.contentLength = length;
	}
	public  int contentLength() {
		return this.contentLength;
	}

	@Override
	public void header(String header, String value) {
		// TODO Auto-generated method stub
		Header+=header+":"+value+CRLF;
		
		
	}

	@Override
	public void redirect(String location) {
		// TODO Auto-generated method stub
		
		this.status(302);
		header("Location", location);
		
	}

	@Override
	public void redirect(String location, int httpStatusCode) {
		// TODO Auto-generated method stub
		
		this.status(httpStatusCode);
		header("Location", location);
		
		
		
	}

	@Override
	public void cookie(String name, String value) {
		// TODO Auto-generated method stub
		HashMap<String, String> directives  =new HashMap<String, String>();
		directives.put("value", value);
		CookieMap.put(name, directives);
		
	}

	@Override
	public void cookie(String name, String value, int maxAge) {
		// TODO Auto-generated method stub
		HashMap<String, String> directives  =new HashMap<String, String>();
		directives.put("value", value);
		directives.put("maxAge", Integer.toString(maxAge));
		
		CookieMap.put(name, directives);
	}

	@Override
	public void cookie(String name, String value, int maxAge, boolean secured) {
		// TODO Auto-generated method stub
		HashMap<String, String> directives  =new HashMap<String, String>();
		directives.put("value", value);
		directives.put("maxAge", Integer.toString(maxAge));
		directives.put("secured", Boolean.toString(secured));
		CookieMap.put(name, directives);
	}

	@Override
	public void cookie(String name, String value, int maxAge, boolean secured, boolean httpOnly) {
		// TODO Auto-generated method stub
		HashMap<String, String> directives  =new HashMap<String, String>();
		directives.put("value", value);
		directives.put("maxAge", Integer.toString(maxAge));
		directives.put("secured", Boolean.toString(secured));
		directives.put("httpOnly", Boolean.toString(httpOnly));
		CookieMap.put(name, directives);
		
	}

	@Override
	public void cookie(String path, String name, String value) {
		// TODO Auto-generated method stub
		HashMap<String, String> directives  =new HashMap<String, String>();
		directives.put("value", value);
		directives.put("path", path);
		
		CookieMap.put(name, directives);
	}

	@Override
	public void cookie(String path, String name, String value, int maxAge) {
		// TODO Auto-generated method stub
		HashMap<String, String> directives  =new HashMap<String, String>();
		directives.put("value", value);
		directives.put("path", path);
		directives.put("maxAge", Integer.toString(maxAge));
		CookieMap.put(name, directives);
		
	}

	@Override
	public void cookie(String path, String name, String value, int maxAge, boolean secured) {
		// TODO Auto-generated method stub
		HashMap<String, String> directives  =new HashMap<String, String>();
		directives.put("value", value);
		directives.put("path", path);
		directives.put("maxAge", Integer.toString(maxAge));
		directives.put("secured", Boolean.toString(secured));
		
		CookieMap.put(name, directives);
	}

	@Override
	public void cookie(String path, String name, String value, int maxAge, boolean secured, boolean httpOnly) {
		// TODO Auto-generated method stub
		HashMap<String, String> directives  =new HashMap<String, String>();
		logger.debug("In cookie");
		logger.debug(path+" "+name+" "+value);
		
		logger.debug(directives.put("value", value));
		directives.put("path", path);
		directives.put("maxAge", Integer.toString(maxAge));
		directives.put("secured", Boolean.toString(secured));
		directives.put("httpOnly", Boolean.toString(httpOnly));
		CookieMap.put(name, directives);
	}

	@Override
	public void removeCookie(String name) {
		// TODO Auto-generated method stub
		//CookieMap.remove(name);
		
		HashMap<String, String> directives  =new HashMap<String, String>();
		HashMap<String, String> cookieMap=CookieMap.get(("name"));
		if(cookieMap!=null){
			cookieMap.put("maxAge", Integer.toString(0));
		}
		else{
			directives.put("value", "");
			directives.put("maxAge", Integer.toString(0));
			CookieMap.put(name, directives);
		}
		
		
	}

	@Override
	public void removeCookie(String path, String name) {
		// TODO Auto-generated method stub
		
		CookieMap.get(("name")).put("maxAge", Integer.toString(0));
	}

}
