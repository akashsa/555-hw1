package edu.upenn.cis.cis455.m1.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ListenerThread  implements Runnable{
	static final Logger logger = LogManager.getLogger(ListenerThread.class);
	ArrayList<Thread> workerThreads=null;
	public ServerSocket serverSocket=null;
	public Socket server=null;
	String ipAddress=null;
	int port;
	boolean unitTest=false;
	public HttpTaskQueue requestQueue;

	HttpServer httpserver=null;
	public ListenerThread(HttpServer httpserver, ServerSocket s,HttpTaskQueue requestQueue, ArrayList<Thread> workerThreads, String ipAddress, int port) {
		// TODO Auto-generated constructor stub
		this.serverSocket=s;
		this.ipAddress=ipAddress;
		this.workerThreads=workerThreads;
		this.port=port;
		this.httpserver=httpserver;
		this.requestQueue= requestQueue;
	}
	
	@Override
	public void run() {
		logger.debug("<><><><><><><><><in listener thread><><><><><><><><><>"+httpserver.threadPoolSize);
		try{
				 
				logger.debug("<><><><><><><><><blah><><><><><><><><><>"+httpserver.threadPoolSize);
				for (int j = 0; j < httpserver.threadPoolSize; j++) {
	
					Thread workerThread = new Thread(new HttpWorker(httpserver));
	
					//logger.debug("starting thread"+ workerThreads.get(j).getId());
					workerThreads.add(workerThread);
					httpserver.StatusMap.put(workerThread.getName(), "WAITING");
	
				}
				for(int j = 0; j < workerThreads.size(); j++){
					workerThreads.get(j).setDaemon(true);
					workerThreads.get(j).start();
				}


				
				
				
				System.out.println("Listening on "+ port);
	
				//serverSocket.setSoTimeout(10000);
				while(httpserver.isActive()){
					//setDaemon(true) 
	
					logger.debug("Just connected to " );
					try{
						server = serverSocket.accept();
						
						HttpTask task= new HttpTask(server);// wrapper for socket it is basically a socket
						
						
						requestQueue.add(task);
						
						logger.debug("Just connected to " + server.getRemoteSocketAddress() + task.getSocket().getRemoteSocketAddress());
	
					}
					catch(SocketTimeoutException | InterruptedException  | SocketException e){
						
						logger.debug("Caught SocketTimeoutException "+ Thread.currentThread().getName());
						//e.printStackTrace();
	
					} finally {
						//
					}
				}
		
	}catch(Exception e){
		e.printStackTrace();
	}
		finally{
			httpserver.isActive(false);
		}

}

}
