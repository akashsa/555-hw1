package edu.upenn.cis.cis455.m1.server.implementations;


import java.io.BufferedOutputStream;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import edu.upenn.cis.cis455.RouteMap;
import edu.upenn.cis.cis455.ServiceFactory;
import edu.upenn.cis.cis455.WebServer;
import edu.upenn.cis.cis455.WebServiceController;
import edu.upenn.cis.cis455.exceptions.HaltException;
import edu.upenn.cis.cis455.m1.server.HttpIoHandler;
import edu.upenn.cis.cis455.m2.server.interfaces.Request;
import edu.upenn.cis.cis455.m2.server.interfaces.Session;

public class RequestImplementation extends Request {
	static Logger logger = LogManager.getLogger(RequestImplementation.class);
	String requestMethod;
	String host;
	String userAgent;
	int port;
	String pathInfo;
	String uri;
	String url;
	String protocol;
	String contentType;
	String ip;
	String body;
	Map<String,String> header; 
	Map<String, List<String>> parms;
	HashMap<String, String> cookieMap=new HashMap<String, String>();
	HashMap<String, Object> attributeMap=new HashMap<String, Object>();
	int contentLength;
	HashMap<String, String> parMap = new HashMap<String, String>();
	public String sessionID=null;
	
	public RequestImplementation(Map<String, String> headers, Map<String, List<String>> parms2, String uri, int port, String ip )throws HaltException{
		/*{accept-language=en-GB,en-US;q=0.8,en;q=0.6, cookie=JSESSIONID=990EB514D6B4FBC319FBF86045F8BB9B, Method=GET, accept=text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*//*;q=0.8, remote-addr=/0:0:0:0:0:0:0:1:50233, http-client-ip=/0:0:0:0:0:0:0:1:50233, host=localhost:8081, upgrade-insecure-requests=1, connection=keep-alive, protocolVersion=HTTP/1.1, cache-control=max-age=0, accept-encoding=gzip, deflate, br, user-agent=Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36}
/*

http://www.example.com/docs/resource1.html?param1=value1
 
The methods will return the following:

1) pathInfo() - /docs/resource1.html
2) url() - http://www.example.com/docs/resource1.html?param1=value1
3) uri() - http://www.example.com/docs/resource1.html
*/
		
	//URI: http://my.url/path
		//URI: path
		//URI: my.url/path
		
		//host=my.url
		if(headers.get("host")==null){
			
		}

		
		
		String[] x =uri.split(headers.get("host"));
		logger.debug("X:"+ Arrays.toString(x));

		if(x[0].startsWith("http")){
			
			
			this.url=x[0]+headers.get("host")+x[1];
			this.pathInfo=x[1].split("\\?")[0];
			this.uri=x[0]+headers.get("host")+this.pathInfo;
			
			
			logger.debug("URL:"+this.url);
			logger.debug("URI:"+this.uri);
			logger.debug("PathInfo:"+this.pathInfo);
		}
		else if(x.length>1){
			
			this.url="http://"+headers.get("host")+x[1];
			this.pathInfo=x[1].split("\\?")[0];

			this.uri="http://"+headers.get("host")+this.pathInfo;
			
			logger.debug("URL:"+this.url);
			logger.debug("URI:"+this.uri);
			logger.debug("PathInfo:"+this.pathInfo);
			
		}
		else {
			
			this.url="http://"+headers.get("host")+x[0];
			this.pathInfo=x[0].split("\\?")[0];

			this.uri="http://"+headers.get("host")+this.pathInfo;
			
			logger.debug("URL:"+this.url);
			logger.debug("URI:"+this.uri);
			logger.debug("PathInfo:"+this.pathInfo);
			
		}
		this.parms=parms2;
		this.requestMethod= headers.get("Method");
		this.host=headers.get("host");
		this.protocol=headers.get("protocolVersion");
		this.userAgent=headers.get("user-agent");
		this.port=port;
		this.ip=ip;
		this.contentType=headers.get("Content-Type"); //Content-Type is added by Http worker by calling getMIME type
		this.header= headers;
		System.out.println("--------------------------------------");
		System.out.println("----------------Headers---------------");
		System.out.println("--------------------------------------");
		System.out.println(headers);
		this.parms=parms2;
		this.cookies();
		RouteMap rm =WebServiceController.getMap();
		Set<String> KeySet=rm.keySet(this.requestMethod());
		
		Iterator<String> KeySetIter=KeySet.iterator();
		
		String pathInfo=this.pathInfo();

		
		while(KeySetIter.hasNext()){
			logger.debug("KeySetIter.hasNext()");
			String getRouteKey=KeySetIter.next();
			String[] pathInfoArray=this.pathInfo().split("/");
			String[] param= getRouteKey.split("/");
			logger.debug("param . length---------"+param.length);
			if(pathInfoArray.length>=param.length){
				for(int i = 0;i<param.length;i++){
					if(param[i].startsWith(":")){
						logger.debug("this is the param map values"+param[i]+"="+pathInfoArray[i]);
						parMap.put(param[i], pathInfoArray[i]);
						
					}
				}
			}

		}
		
		
		
		
	}





	@Override
	public String requestMethod() {
		// TODO Auto-generated method stub
		
		
		return this.requestMethod;
	}

	@Override
	public String host() {
		// TODO Auto-generated method stub
		return this.host;
	}

	@Override
	public String userAgent() {
		// TODO Auto-generated method stub
		return this.userAgent;
	}

	@Override
	public int port() {
		// TODO Auto-generated method stub
		return this.port;
	}

	@Override
	public String pathInfo() {
		// TODO Auto-generated method stub
		return this.pathInfo;
	}

	@Override
	public String url() {
		// TODO Auto-generated method stub
		return this.url;
	}
	
	public void url(String s) {
		// TODO Auto-generated method stub
		 this.url=s;
	}
	public void uri(String s) {
		// TODO Auto-generated method stub
		 this.uri=s;
	}
	
	@Override
	public String uri() {
		// TODO Auto-generated method stub
		return this.uri;
	}

	@Override
	public String protocol() {
		// TODO Auto-generated method stub
		return this.protocol;
	}

	@Override
	public String contentType() {
		// TODO Auto-generated method stub
		return this.contentType;
	}

	@Override
	public String ip() {
		// TODO Auto-generated method stub
		return this.ip;
	}

	@Override
	public String body() {
		// TODO Auto-""generated method stub

		
		return this.header.get("body");
	}

	@Override
	public int contentLength() {
		String lengthString=header.get("Content-Length");

		try  
		  {  

		    int d = Integer.parseInt(lengthString);
		    return d;
		  }  
		  catch(NumberFormatException nfe)  
		  {  
		    return 0;  
		  } 
		
	}

	@Override
	public String headers(String name) {
		// TODO Auto-generated method stub
		return header.get(name);
	}

	@Override
	public Set<String> headers() {
		// TODO Auto-generated method stub
		return header.keySet();
	}
	public void pathInfo(String s) {
		// TODO Auto-generated method stub
		this.pathInfo=s;
		return;
	}


/**********************<Milestone 2>***************************/
	/**
	 * @return Gets the session associated with this request
	 */
	public  Session session(){
		if(this.cookies()!=null){
		HashMap<String, String> cookie= this.cookies();
		if(cookie.get("JSESSIONID")!=null){
			this.sessionID=cookie.get("JSESSIONID");
			return WebServiceController.getSessionMap().get(this.sessionID);
		}
		else{
			return null;
		}
		}
		
		else{
			return null;
		}
	}

	/**
	 * @return Gets or creates a session for this request
	 */
	public  Session session(boolean create){
		
		if(create){
			this.sessionID= ServiceFactory.createSession();
			
			return ServiceFactory.getSession(this.sessionID);
		}
		
		else{
			return null;
		}
		
		
	}

	/**
	 * @return a map containing the route parameters
	 */
	public  HashMap<String, String> params(){
		
		
		
		return parMap;
	}

	/**
	 * @return the named parameter Example: parameter 'name' from the following
	 *         pattern: (get '/hello/:name')
	 */
	public String params(String param) {
		if (param == null)
			return null;

		if (param.startsWith(":"))
			return params().get(param.toLowerCase());
		else
			return params().get(':' + param.toLowerCase());
	}

	/**
	 * @return Query parameter from the URL
	 */
	public  String queryParams(String param){
		return parms.get(param).toString();
	}

	public String queryParamOrDefault(String param, String def) {
		String ret = queryParams(param);

		return (ret == null) ? def : ret;
	}

	/**
	 * @return Get the list of values for the query parameter
	 */
	public  List<String> queryParamsValues(String param){
		return parms.get(param);
	}

	public  Set<String> queryParams(){
		return parms.keySet();
	}

	/**
	 * @return The raw query string
	 */
	public  String queryString(){
		return parms.toString();
	}

	/**
	 * Add an attribute to the request (eg in a filter)
	 */
	public  void attribute(String attrib, Object val) {
		this.attributeMap.put(attrib, val);
	}

	/**
	 * @return Gets an attribute attached to the request
	 */
	public  Object attribute(String attrib){
		return this.attributeMap.get(attrib);
	}

	/**
	 * @return All attributes attached to the request
	 */
	public  Set<String> attributes(){
		return this.attributeMap.keySet();
		
	}

	public  HashMap<String, String> cookies(){
	String s=this.header.get("cookie");
	if(s!=null){
	logger.debug("_________________________________the debugger string______________");
	logger.debug(s);
	String s1[]=s.split(";");

	logger.debug(s1[0]);	logger.debug(s1[1]);	logger.debug(s1[2]);
	for(String i: s1){
		String Key[]= i.split("=");
		logger.debug(Key[0]);
		logger.debug(Key[1]);
		cookieMap.put(Key[0], Key[1]);
		
	}
	
		return cookieMap;
	}
	else{return null;}
	}

	public String cookie(String name) {
		if (name == null || cookies() == null)
			return null;
		else
			return cookies().get(name);
	}
    
}
