package edu.upenn.cis.cis455.m1.server;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import static edu.upenn.cis.cis455.ServiceFactory.*;

import edu.upenn.cis.cis455.ServiceFactory;
import edu.upenn.cis.cis455.exceptions.HaltException;
import edu.upenn.cis.cis455.m1.server.interfaces.Request;
import edu.upenn.cis.cis455.m1.server.interfaces.Response;
import edu.upenn.cis.cis455.util.HttpParsing;

/**
 * Handles marshalling between HTTP Requests and Responses
 */
public class HttpIoHandler {
    final static Logger logger = LogManager.getLogger(HttpIoHandler.class);

    /**
     * Sends an exception back, in the form of an HTTP response code and message.  Returns true
     * if we are supposed to keep the connection open (for persistent connections).
     * @throws IOException 
     */
    public static boolean sendException(Socket socket, Request request, HaltException except) throws IOException {
    	try{
    	int length=0;
    	if(except.body() !=null){
    	length=except.body().getBytes().length;
    	}
    	BufferedOutputStream bs=null;
		 SimpleDateFormat currentTimesdf= new SimpleDateFormat("EEE, dd MMM, yyyy HH:mm:ss z");
		 Date currentTime= new Date();
		currentTimesdf.setTimeZone(TimeZone.getTimeZone("GMT"));

    	
		try {
			 bs = new BufferedOutputStream(socket.getOutputStream());
			
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
//    	
//    	if(request.headers("expect")!=null){
//    		
//    		String Continue= new String("HTTP/1.1 100 continue");
//    		
//    		try {
//				//socket.getOutputStream().write(Continue.getBytes());
//				//socket.getOutputStream().flush();
//    			
//    			
//    			bs.write(Continue.getBytes());
//    			bs.flush();
//			} catch (IOException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//    		
//    	}
    	
    	logger.debug("code "+except.statusCode()+" "+except.getMessage()+" "+ Thread.currentThread().getName());
    	
    	String s ="HTTP/1.1 "+except.statusCode()+" "+ except.getMessage()+"\r\n"+
    				"Date: "+currentTimesdf.format(currentTime)+"\r\n"+
    				"Content-Type: text/html"+"\r\n"+
    				"Content-Length: "+length+"\r\n"
    				+"\r\n";
    				
    				
    	if(length!=0){
    		s+=except.body()+"\r\n";
    	}
    				
    	
    	logger.debug("the exception is "+s);

    	byte[] b = s.getBytes();
//    	socket.getOutputStream().write(b);
//		socket.getOutputStream().flush();
//    	socket.getOutputStream().close();
    	bs.write(b);
		bs.flush();
    	bs.close();
    	socket.close();
    	bs.close();
    	return true;
    	}catch(IOException e){
    		logger.debug("Send exception error in "+Thread.currentThread().getName());
    		return false;
    	}
    	
    	
    	
    }

    /**
     * Sends data back.   Returns true if we are supposed to keep the connection open (for 
     * persistent connections).
     */
    public static boolean sendResponse(Socket socket, Request request, Response response){
    	String Message =null;

    	
    	switch(response.status() ){
    	case 200:	Message="OK";
    				break;
    	case 404:	Message="Not Found";   				
    				break;
    	default:	Message="";			
    	}
    	
    	
    	BufferedOutputStream bs=null;
    	
		try {
			 bs = new BufferedOutputStream(socket.getOutputStream());
			
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
    	
    	
    	if(request.headers("expect")!=null){
    		
    		String Continue= new String("HTTP/1.1 100 continue");
    		
    		try {
				//socket.getOutputStream().write(Continue.getBytes());
        		bs.write(Continue.getBytes());
        		bs.flush();
    			
    			
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    		
    		
    	}
    	
    	String res="HTTP/1.1 "+ response.status()+" "+Message+"\r\n"+ response.getHeaders();
    	logger.debug(res);
    	
    	
    	
    	
    	try {
    		
    		
    		
			//socket.getOutputStream().write(res.getBytes());
			
    		
    		bs.write(res.getBytes());
    		bs.flush();
			if(response.bodyRaw()!=null){
				//socket.getOutputStream().write(response.bodyRaw());
	    		bs.write(response.bodyRaw());
	    		
	    		
					bs.flush();
					bs.close();
			}
			
			return true;
		} catch (IOException e) {
			
		
			// TODO Auto-generated catch block
			//e.printStackTrace();
			return false;
		}
        
    }
}
