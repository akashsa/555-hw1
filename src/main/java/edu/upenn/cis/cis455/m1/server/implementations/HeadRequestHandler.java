package edu.upenn.cis.cis455.m1.server.implementations;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Set;

import edu.upenn.cis.cis455.WebServiceController;
import edu.upenn.cis.cis455.exceptions.HaltException;
import edu.upenn.cis.cis455.m1.server.interfaces.HttpRequestHandler;
import edu.upenn.cis.cis455.m2.server.interfaces.Request;
import edu.upenn.cis.cis455.m2.server.interfaces.Response;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public class HeadRequestHandler implements HttpRequestHandler {
static final Logger logger = LogManager.getLogger(HeadRequestHandler.class);
	@Override




	public void handle(Request request, Response response) throws HaltException {



		InputStream fs=null;
		String root=WebServiceController.directory();
	
		
		RequestImplementation req=(RequestImplementation)request;
		
        logger.debug("req file "+root+( request).pathInfo()+"index.html");



		if(request.pathInfo().contentEquals("/")){

			File file=null;
			file= new File(root+(request).pathInfo()+"index.html");
			try {
				fs= new FileInputStream(file);
			} catch (FileNotFoundException e) {
				//String body="<html><head><title>Fail!!</title></head><body><h1>HTTP/1.1</h1><p>file not found</p></body></html>";
				
				logger.debug("*****File not Found ******");
				HaltException x =new HaltException(404);
				throw x;
			}

			try {
				fs.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block

			}
			response.contentLength((int)file.length());
			response.body(null);
			response.status(200);

			response.type("text/html");


		}

		else{
			File file= new File(root+( request).pathInfo());
			logger.debug("req file "+root+( request).pathInfo());
			try {
				fs= new FileInputStream(file);
			} catch (FileNotFoundException e) {
            

				//String body="<html><head><title>Fail!!</title></head><body><h1>HTTP/1.1</h1><p>file not found</p></body></html>";
				HaltException x =new HaltException(404);
				throw x;

			}
			try {
				fs.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block

			}
			response.contentLength((int)file.length());
			response.body(null);
			response.status(200);
			response.type(request.contentType());

		}

















	}


}
