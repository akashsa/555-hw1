package edu.upenn.cis.cis455.m1.server.implementations;

import java.util.Date;
import java.util.HashMap;
import java.util.Set;

import edu.upenn.cis.cis455.ServiceFactory;
import edu.upenn.cis.cis455.WebServiceController;
import edu.upenn.cis.cis455.m2.server.interfaces.Session;

public class SessionImplementation extends Session {
	HashMap<String, Object>sessionAttributes=new HashMap<String, Object>();
	final int MAXINACTIVEINTERVAL=10;
	long creationTime=0;
	long lastAccessedTime=0;
	int maxInactiveInterval=0;
	String sessionID=null;

	public SessionImplementation(String ID) {
		// TODO Auto-generated constructor stub
		this.maxInactiveInterval=MAXINACTIVEINTERVAL;
		this.creationTime=ServiceFactory.getCurrentTimeMs();
		this.lastAccessedTime=this.creationTime;
		this.sessionID=ID;
		
	}

	@Override
	public String id() {
		// TODO Auto-generated method stub
		return this.sessionID;
	}

	@Override
	public long creationTime() {
		// TODO Auto-generated method stub
		return this.creationTime;
	}

	@Override
	public long lastAccessedTime() {
		// TODO Auto-generated method stub
		return lastAccessedTime;
	}

	@Override
	public synchronized void invalidate() {
		// TODO Auto-generated method stub
		WebServiceController.getSessionMap().remove(this.sessionID);
		

	}

	@Override
	public int maxInactiveInterval() {
		// TODO Auto-generated method stub
		return maxInactiveInterval;
	}

	@Override
	public void maxInactiveInterval(int interval) {
		// TODO Auto-generated method stub
		this.maxInactiveInterval=interval;
	}

	@Override
	public void access() {
		// TODO Auto-generated method stub
		this.lastAccessedTime=ServiceFactory.getCurrentTimeMs();

	}

	@Override
	public synchronized void attribute(String name, Object value) {
		
		// TODO Auto-generated method stub
		sessionAttributes.put(name, value);

	}

	@Override
	public Object attribute(String name) {
		// TODO Auto-generated method stub
		return sessionAttributes.get(name);
	}
    /**
     * Get all objects bound to the session
     */
	@Override
	public Set<String> attributes() {
		// TODO Auto-generated method stub
		
		return sessionAttributes.keySet();
	}

	@Override
	public synchronized void removeAttribute(String name) {
		// TODO Auto-generated method stub
		sessionAttributes.remove(name);
	}

}
