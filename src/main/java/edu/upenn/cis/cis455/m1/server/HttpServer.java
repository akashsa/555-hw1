package edu.upenn.cis.cis455.m1.server;

import java.io.IOException;
import java.lang.Thread.State;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Queue;
import java.util.Vector;



import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.jmx.Server;

import edu.upenn.cis.cis455.WebServer;

/**
 * Stub for your HTTP server, which
 * listens on a ServerSocket and handles
 * requests
 */
public class HttpServer implements ThreadManager  {
	static final Logger logger = LogManager.getLogger(HttpServer.class);
	public ServerSocket serverSocket=null;
	public Socket server=null;
	public HttpTaskQueue requestQueue;
	public int threadPoolSize;
	public String root=null;
	public ArrayList<Thread> alist = new ArrayList<>();
	boolean unitTest=false;
	boolean isActive=true;
	public String ipAddress= null;

	public ArrayList<Thread> workerThreads=null;
	public Map<String, String> StatusMap=new HashMap<String, String>();
	int port=0;
	public HttpServer(int threadPoolSize, int requestQueueSize, String root){
		this.requestQueue = new HttpTaskQueue(requestQueueSize);
		this.threadPoolSize=threadPoolSize;
		this.root=root;

	}
	public HttpServer(int threadPoolSize, int requestQueueSize, String root, boolean unitTest, Socket incoming){
		this.requestQueue = new HttpTaskQueue(requestQueueSize);
		
		this.threadPoolSize=threadPoolSize;
		this.root=root;
		this.unitTest=unitTest;
		this.server=incoming;
	}


	public void initializeHttpServer(int port, String ipAddress) throws IOException, InterruptedException{

		try{	this.port=port;
				this.ipAddress=ipAddress;
				workerThreads = new ArrayList<Thread>(this.threadPoolSize);
//	
//				for (int j = 0; j < this.threadPoolSize; j++) {
//	
//					Thread workerThread = new Thread(new HttpWorker(this));
//	
//					//logger.debug("starting thread"+ workerThreads[j].getId());
//					workerThreads.add(workerThread);
//					StatusMap.put(workerThread.getName(), "WAITING");
//	
//				}
//				for(int j = 0; j < workerThreads.size(); j++){
//					workerThreads.get(j).setDaemon(true);
//					workerThreads.get(j).start();
//				}
//
				try {
					InetAddress inetAddress=InetAddress.getByName(ipAddress);
					System.out.println(inetAddress);
					serverSocket = new ServerSocket(port,50,inetAddress);
				} catch (IOException e) {
					
					e.printStackTrace();
				}
//				
//				//Listener Thread
				logger.debug("<<<<<<<<<<<<<<<inside http server>>>>>>>>>>>>>>>>");
				Thread listenerThread = new Thread(new ListenerThread(this, this.serverSocket, this.requestQueue,this.workerThreads, this.ipAddress,port));
				logger.debug("<<<<<<<<<<<<<<<starting thread>>>>>>>>>>>>>>>>");
				listenerThread.start();
//				if(unitTest==true){
//					//server = serverSocket.accept();
//					HttpTask task= new HttpTask(server);
//					requestQueue.add(task);
//					logger.debug("In unit test " + server.getRemoteSocketAddress() + task.getSocket().getRemoteSocketAddress());
//	
//				}
	
				//serverSocket.setSoTimeout(10000);
//				while(unitTest!=true && this.isActive()){
//					//setDaemon(true) 
//	
//	
//					try{
//						server = serverSocket.accept();
//						
//						HttpTask task= new HttpTask(server);// wrapper for socket it is basically a socket
//						
//						
//						requestQueue.add(task);
//						
//						logger.debug("Just connected to " + server.getRemoteSocketAddress() + task.getSocket().getRemoteSocketAddress());
//	
//					}
//					catch(SocketTimeoutException | InterruptedException e){
//						
//						logger.debug("Caught SocketTimeoutException "+ Thread.currentThread().getName());
//						//e.printStackTrace();
//	
//					} finally {
//					}
//				}
//				logger.debug("******************<Socket exiting>***********");
		}

		catch(Exception e){e.printStackTrace();}
		finally{
//			if(this.isActive()!=true){
//
//				for(int j = 0; j < workerThreads.size(); j++){
//	
//					if(workerThreads.get(j).getState()==State.WAITING){	
//						workerThreads.get(j).interrupt();
//					}
//	
//	
//				}
//				for(int j = 0; j < workerThreads.size(); j++){
//	
//	
//					workerThreads.get(j).join();
//					logger.debug(workerThreads.get(j)+"joined");
//	
//	
//				}
//			}
		}
	}


	@Override
	public HttpTaskQueue getRequestQueue() {

		// TODO Auto-generated method stub
		return requestQueue;
	}

	public Map getStatusMap() {

		// TODO Auto-generated method stub
		return this.StatusMap;
	} 

	public ArrayList<Thread> aList() {

		// TODO Auto-generated method stub
		return this.workerThreads;
	} 

	@Override
	public boolean isActive() {
		// TODO Auto-generated method stub
		return this.isActive;
	}
	public void isActive(boolean isActive){
		this.isActive=isActive;
	}

	@Override
	public void start(HttpWorker worker) {
		// TODO Auto-generated method stub


	}
	public void start(HttpWorker worker, String uri) {
		// TODO Auto-generated method stub
		logger.debug(serverSocket.getInetAddress());

		this.StatusMap.replace(Thread.currentThread().getName(), serverSocket.getInetAddress()+":"+serverSocket.getLocalPort()+uri);  


	}

	@Override
	public void done(HttpWorker worker) {
		// TODO Auto-generated method stub
		this.StatusMap.replace(Thread.currentThread().getName(), "WAITING");  

	}

	@Override
	public void error(HttpWorker worker) {





		// TODO Auto-generated method stub

	}
}
