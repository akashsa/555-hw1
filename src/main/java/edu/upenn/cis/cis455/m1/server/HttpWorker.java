package edu.upenn.cis.cis455.m1.server;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import edu.upenn.cis.cis455.FilterStructure;
import edu.upenn.cis.cis455.ServiceFactory;
import edu.upenn.cis.cis455.WebServiceController;
import edu.upenn.cis.cis455.exceptions.HaltException;
import edu.upenn.cis.cis455.handlers.Filter;
import edu.upenn.cis.cis455.m1.server.implementations.GetRequestHandler;
import edu.upenn.cis.cis455.m1.server.implementations.HttpRequestHandlerImplementation;
import edu.upenn.cis.cis455.m2.server.interfaces.Request;
import edu.upenn.cis.cis455.m2.server.interfaces.Response;
import edu.upenn.cis.cis455.util.HttpParsing;

/**
 * Stub class for a thread worker for
 * handling Web requests
 */
public class HttpWorker implements Runnable{
	final static Logger logger = LogManager.getLogger(HttpWorker.class);
	HttpServer httpServer;
	HttpTaskQueue requestQueue;
	Request request=null;
	String root=null;
	public HttpWorker(HttpServer s){
		this.httpServer=s;
		requestQueue = s.getRequestQueue();
		this.root=s.root;
	}
	HttpTask task=null;
	@Override
	public void run() {

		// TODO Auto-generated method stub
		logger.debug("Now In HTTPWorker I have to deque request from the shared blocking queue");
		while(httpServer.isActive() && (Thread.currentThread().isInterrupted()!=true)){
			try {

				
				task =requestQueue.remove(0);
				
				if(!httpServer.isActive()){
					synchronized(requestQueue){
						requestQueue.notifyAll();
					}
					logger.debug("parent dead check");
					continue;
				}
				
				logger.debug("got connection in worker to"+ task.requestSocket.getRemoteSocketAddress()
				+":" + task.requestSocket.getPort() + Thread.currentThread().getId()+Thread.currentThread().getName()	);

				Map<String, String> headers = new HashMap<String, String>();
				Map<String, List<String>> parms=new HashMap<String, List<String>>();
				BufferedInputStream bis=new BufferedInputStream(task.getSocket().getInputStream());
				logger.debug(task.getSocket().getRemoteSocketAddress().toString() +" "+Thread.currentThread().getName());


				String uri=HttpParsing.parseRequest(task.getSocket().getRemoteSocketAddress().toString()
						, bis, headers, parms);
				if(headers.get("host")==null){
					logger.debug("*********************<Host=null>*********************");
					HaltException e= new HaltException(400, null, "host cannot be empty");
					throw e;
				}
				else{
				    logger.debug("host = "+headers.get("host"));
				}
				httpServer.start(this, uri);//change state


				logger.debug(uri);
				logger.debug("uri is "+uri);
				
				
				String ContentType=HttpParsing.getMimeType(uri);

				headers.put("Content-Type", ContentType);


				logger.debug(headers);
				logger.debug(parms);
				request=ServiceFactory.createRequest(task.getSocket(), uri, false, headers, parms);
				

				
				Response response=ServiceFactory.createResponse();//returns an empty response object
				SpecialURL(request);
				
				FilterStructure before = WebServiceController.getBeforeStruct();
				logger.debug("size of before ="+before.filterVector.size());
				FilterStructure after = WebServiceController.getAfterStruct();
				logger.debug("outside before");
				logger.debug(before.filterVector.size());
				
				logger.debug(before.getIterator().hasNext());
				Iterator<Filter> beforeIter=before.getIterator();
				
				while(beforeIter.hasNext()){
					
					logger.debug("getting default before filters");
					beforeIter.next().handle(request, response);
					
				}
				Filter f=before.get(request.requestMethod(), request.pathInfo());
				if(f!=null){
					
					f.handle(request, response);					
				}
				
				HttpRequestHandlerImplementation requestHandler= (HttpRequestHandlerImplementation) ServiceFactory.createRequestHandlerInstance(Paths.get(WebServiceController.directory()));
				if(requestHandler !=null){
					logger.debug("Trying to match");
					requestHandler.handle(request, response);
				}
		
				Iterator<Filter>afterIter=before.getIterator();
				while(afterIter.hasNext()){
					System.out.println("inside after");
					afterIter.next().handle(request, response);
					
				}
				Filter afterFilter=after.get(request.requestMethod(), request.pathInfo());
				
				if(afterFilter!=null){
					afterFilter.handle(request, response);
				}
			
				
				HttpIoHandler.sendResponse(task.getSocket(), request, response);
				

				


				response=null;
				request=null;
				parms=null;
				headers=null;
				task=null;

				//task.requestSocket.close();
				httpServer.done(this);




			} catch (InterruptedException e) {
				httpServer.done(this);
				logger.debug("Interrupted exception in"+Thread.currentThread().getName());
				
			} catch (HaltException e) {
				httpServer.done(this);
				// TODO Auto-generated catch block

				try {
					HttpIoHandler.sendException(task.getSocket(),request, e);
					task.getSocket().close();
				} catch (IOException e1) {
					logger.debug("Socket closed exception in"+Thread.currentThread().getName());
					// TODO Auto-generated catch block
					e1.printStackTrace();
					
				}


			} catch (IOException e) {
				httpServer.done(this);
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			finally{
				httpServer.done(this);
				if(httpServer.isActive()!=true){
					try {
						if(task!=null)
						task.getSocket().close();
					} catch (IOException e) {
						logger.debug("Socket closed exception in"+Thread.currentThread().getName());
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					catch (NullPointerException e) {
						logger.debug("Null Ptr"+Thread.currentThread().getName());
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}

		}
	}

	
public static void RequestValidation(Request request, HttpServer httpServer) throws HaltException{
		
		
		
		
		if(request.headers("host")==null){
			System.out.println("*********************<Host=null>*********************");
			HaltException e= new HaltException(400, null, "host cannot be empty");
			throw e;
		}
		
		String[] x=(request.pathInfo().split(request.headers("host")));
		
		logger.debug(Arrays.toString(x));
		//logger.debug("this "+(request.pathInfo().split("/")[1]));
		
		
		
		
		logger.debug(x.length);
		logger.debug(request.pathInfo());
		if(x.length>1){
			request.pathInfo(x[1]);
		}
		logger.debug(request.pathInfo());
		
		

		//Host header missing
		String protocolVersion= new String();
		//s=request.headers("host");
		
		String requestPathString= new String(WebServiceController.directory());
	
		requestPathString+=request.pathInfo();//request
		
		Path requestPath= Paths.get(requestPathString);
		
		requestPath=requestPath.toAbsolutePath().normalize();
		System.out.println(requestPath);
		String path=requestPath.toString();
		
		
		String rootString=new String( WebServiceController.directory());
		Path root= Paths.get(rootString);
		root=root.toAbsolutePath().normalize();
		
		System.out.println("root= "+root);
		System.out.println(WebServiceController.directory());
		
		System.out.println("path"+path);
		//Path dir2= Paths.get(rootdir2);
		System.out.println("**************"+path.startsWith(root.toString())+"********************");
		
		
		
		//check host == serveraddress and port
		if(request.headers("host").equals("localhost:"+httpServer.serverSocket.getLocalPort()) !=true){
			if(request.headers("host").equals(httpServer.serverSocket.getInetAddress().toString()+":"+httpServer.serverSocket.getLocalPort())!=true){
				HaltException e = new HaltException(400,null,"Bad dssrequest");
				throw e;
				
			}
		}
		
		
		
		
		
		protocolVersion=request.headers("protocolVersion");
		logger.debug(protocolVersion);
		 if(protocolVersion.equals("HTTP/1.1")!=true){
			System.out.println("*********************<protocolVersion.equals(\"HTTP/1.1\")!=true>*********************");
			// HTTP/1.0 throw code 505 version not supported
			HaltException e= new HaltException(505,null, "Worng Protocol");
			throw e;	
		}

		
		
		if(path.startsWith(root.toString())!=true){
			//absolute path does not exist
			System.out.println("*********************<Forbidden>*********************");
			HaltException e= new HaltException(403,null,"Forbidden");
			throw e;
			
		}
		logger.debug("request.headers(\"date\"):"+request.headers("date"));
		if(request.headers("date")!=null){
			//date in GMT
			checkDateFormat(request.headers("date"));
			
			
			
		}
		
		logger.debug("request.headers(\"If-Modified-Since\"):"+request.headers("if-modified-since"));
		if(request.headers("if-modified-since")!=null){
			
			System.out.println("*********************<If-Modified-Since>*********************");
			SimpleDateFormat sdf = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss z");
			sdf.setTimeZone(TimeZone.getTimeZone("G"));			
			String modifiedDate=request.headers("if-modified-since");
			logger.debug("request.headers(\"modified\"):"+request.headers("if-modified-since"));
			checkDateFormat(modifiedDate);
			Date parsedDate=null;
			Date date=null;
			Date fileDate=null;
			Date systemDate=new Date();
			
			try {
				parsedDate = sdf.parse(modifiedDate);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			String formattedStringDate=sdf.format(parsedDate);
			
			try {
				date= sdf.parse(formattedStringDate);
			} catch (ParseException e) {
				
				e.printStackTrace();
			}

			
			File file = new File(requestPathString);
			
			
			
			System.out.println("After Format : " + sdf.format(file.lastModified()));
			try {
				fileDate= sdf.parse((sdf.format(file.lastModified())));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			if(fileDate.compareTo(date)<=0){
				HaltException e= new HaltException(304, null, "Not Modified");
				throw e;
			}
			else if(date.compareTo(systemDate)>0){
				
				
				//sent date greater than system date. modified in future LOL
				HaltException e= new HaltException(400,null,"Bad request");
				e.getLocalizedMessage();
				throw e;
				
			}
			
			
		}
		logger.debug("request.headers(\"If-Unmodified-Since\"):"+request.headers("If-Unmodified-Since"));
		if(request.headers("If-Unmodified-Since")!=null){
			System.out.println("*********************<If-Unmodified-Since>*********************");
			
			SimpleDateFormat sdf = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss z");
			sdf.setTimeZone(TimeZone.getTimeZone("G"));
			String modifiedDate=request.headers("If-Unmodified-Since");
			checkDateFormat(modifiedDate);
			Date parsedDate=null;
			Date date=null;
			Date fileDate=null;
			Date systemDate=new Date();
			
			try {
				parsedDate = sdf.parse(modifiedDate);
				String formattedStringDate=sdf.format(parsedDate);
				
				date= sdf.parse(formattedStringDate);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			
			File file = new File(requestPathString);
			System.out.println("After Format : " + sdf.format(file.lastModified()));
			try {
				fileDate= sdf.parse((sdf.format(file.lastModified())));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			if(fileDate.compareTo(date)>0){
				HaltException e= new HaltException(412, null, "Precondition Failed");
				throw e;
			}
			else if(date.compareTo(systemDate)>0){
				
				
				//sent date greater than system date. modified in future LOL
				HaltException e= new HaltException(400,null,"Bad request");
				e.getLocalizedMessage();
				throw e;
				
			}
			
			
		}
		
	
		
		
		
		//400 bad request-- malformed request.
		
			
		
		
		
		
		
	}
	public static void checkDateFormat(String date1){
		//String date=request.headers("Date");
		logger.debug("******************<Checking Date>*****************");
		
		SimpleDateFormat sdf = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss z");
		sdf.setTimeZone(TimeZone.getTimeZone("G"));


		Date parsedDate=null;
		try {
			parsedDate= sdf.parse(date1);
			//logger.debug(parsedDate);
			
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//logger.debug(date1);

		logger.debug(sdf.format(parsedDate));
		if(date1.equals(sdf.format(parsedDate))){
			logger.debug(date1);
			logger.debug(sdf.format(parsedDate));
			return;
		}
		else{
			HaltException e= new HaltException(400,null,"Date format wrong");
			throw e;
		}
	}
	
	
	
	public void SpecialURL(Request request) throws InterruptedException{
		
		if(request.pathInfo().equals("/control")){
			
			System.out.println("************In spl URL************");
			
			String html=new String(""
					+ "<!DOCTYPE html>\r\n+"
					+ "<html>\r\n"+
					"\t<head>\r\n"+
					"\t\t<title>Sample File</title>\r\n"+
					"\t\t<head></head>\r\n"+
					"\t\t<body>\r\n"+
					"\t\t\t<table style=\"width:100%\">\r\n" );


			
		
			String line=null;
			String output=null;
			Map<String, String>map=httpServer.getStatusMap();
			
            // FileReader reads text files in the default encoding.
            FileReader fileReader=null;
			try {
				fileReader = new FileReader("target/app.log");
			} catch (FileNotFoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

            // Always wrap FileReader in BufferedReader.
            BufferedReader bufferedReader = 
                new BufferedReader(fileReader);

            try {
				while((line = bufferedReader.readLine()) != null) {
					output +="\n"+line;
				}
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}   

            // Always close files.
            try {
				bufferedReader.close();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			ArrayList<Thread> alist=httpServer.aList();
			for(int i =0; i<alist.size();i++){
				html+="\t\t\t\t<tr>\r\n";
				html+="\t\t\t\t\t<th>"+alist.get(i).getName()+"<th>\r\n";
				html+="\t\t\t\t\t<th>"+map.get(alist.get(i).getName())+"<th>\r\n";
				html+="\t\t\t\t</tr>\r\n";
				
			}

			
			//"<a href=\"localhost:8088/\"><input type=\"button\" value=\"Visit Google\" /></a>"

			html+="\t\t\t</table>\n";
			html+="\t\t<form action=\"/shutdown\" method=\"get\">\n <button style=\"height:200px;width:200px\" type=\"submit\">Shutdown</button>\n</form>";
			//html+="<?php $f = fopen(\"target/app.log\", \"r\"); while(!feof($f)) { echo fgets($f) . \"<br />\";}";
			//html+="fclose($f);?>";
			html+="****************************LOGGER************************\r\n";
			html+=output;
			html+="\t\t</body>\r\n";
			html+="</html>\r\n";

			
			
			
			
			
			
			logger.debug(html);
			HaltException e = new HaltException(200, html, "OK");
			throw e;
			
		}else if(request.pathInfo().equals("/shutdown")){
			
			httpServer.isActive(false);
			try {
				httpServer.serverSocket.close();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				System.out.println("cannot close");
			}
			
			
			Response response=ServiceFactory.createResponse();

				GetRequestHandler requestHandler= new GetRequestHandler();
				requestHandler.handle(request, response);
				logger.debug("populated response");
				HttpIoHandler.sendResponse(task.getSocket(), request, response);
				requestHandler=null;
				//task.requestSocket.getInputStream().close();
				try {
					task.requestSocket.close();
				} catch (IOException e) {
					logger.debug("Couldn't close socket in shutdown request");
				}
			
			
				InterruptedException e= new InterruptedException();
				throw e;
			
			
			
			
			
			
			
			
		}
		else{
			return;
		}
	}
	





}
