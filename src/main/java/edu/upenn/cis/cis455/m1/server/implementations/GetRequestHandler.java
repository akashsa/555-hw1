package edu.upenn.cis.cis455.m1.server.implementations;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import edu.upenn.cis.cis455.WebServiceController;
import edu.upenn.cis.cis455.exceptions.HaltException;
import edu.upenn.cis.cis455.m1.server.HttpServer;
import edu.upenn.cis.cis455.m1.server.interfaces.HttpRequestHandler;
import edu.upenn.cis.cis455.m2.server.interfaces.Request;
import edu.upenn.cis.cis455.m2.server.interfaces.Response;

public class GetRequestHandler implements HttpRequestHandler {
	static final Logger logger = LogManager.getLogger(GetRequestHandler.class);
	@Override
	public void handle(Request request, Response response) throws HaltException{
		InputStream fs=null;
		String root=WebServiceController.directory();
		String fileString=null;
		BufferedReader buf=null;
		RequestImplementation req=(RequestImplementation)request;
		StringBuilder sb=null;




		if(request.pathInfo().contentEquals("/")){
				int c;
				File file=null;
				try {
		
		
						file= new File(root+( request).pathInfo()+"index.html");
						fs= new FileInputStream(file);
						buf = new BufferedReader(new InputStreamReader(fs));
						sb = new StringBuilder();
		
				} 
				catch (FileNotFoundException e) {
						
						String body="<html><head><title>Fail!!</title></head><body><h1>HTTP/1.1</h1><p>file not found</p></body></html>";
						throw new HaltException(404,body);
		
				}
					String line=null;
					try {
							line = buf.readLine();
							while(line !=null){
								sb.append(line).append("\n");
								line = buf.readLine();
			
			
							}
						
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					fileString=sb.toString();
					response.contentLength((int)file.length());
					//String body="<html><head><title>Response</title></head><body><h1>Response</h1><p>test</p></body></html>";
					response.body(fileString);
					logger.debug(fileString);
					response.bodyRaw(fileString.getBytes());
					response.status(200);
					response.type("text/html");
					try {
						fs.close();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
		

		}
		else if(request.pathInfo().contentEquals("/control")){
			
		}
		else if(request.pathInfo().contentEquals("/shutdown")){
			
			String body="<html><head><title>ShutDown</title></head><body><h1>Shutting the server down</h1><img src=\"https://media.tenor.com/images/ae1601098a0123ef9e17432476f8a416/tenor.gif\" width=\"244\" height=\"280\" style=\"background-color: rgb(255, 255, 255); width: 245px; height: 281.148px;\" alt=\"Yippee Ki Yay Motherfucker GIF - Diehard BruceWillis Yippeekiyay GIFs\"></body></html>";
			byte[] b=body.getBytes();
			response.bodyRaw(b);


			response.contentLength(b.length);
			response.status(200);
			response.type("text/html");
			
			
			
			
		}
		else{
			File file= new File(root+( request).pathInfo());
			logger.debug("***************"+root+( request).pathInfo());
			try {
				fs= new FileInputStream(root+( request).pathInfo());
			} catch (FileNotFoundException e) {
				String body="<html><head><title>Fail!!</title></head><body><h1>HTTP/1.1</h1><p>file not found</p></body></html>";
				HaltException x =new HaltException(404,body);
				throw x;



			}
			byte[] b= new byte[(int)file.length()];


			int c=0;
			try {
				c=fs.read((byte[])b);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			response.bodyRaw(b);


			response.contentLength(c);
			response.status(200);
			response.type(request.contentType());

		}











		
		
		
		

	}

}
