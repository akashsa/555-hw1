package edu.upenn.cis.cis455.m1.server.implementations;



import java.awt.RenderingHints.Key;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Path;
import java.util.Iterator;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import edu.upenn.cis.cis455.RouteMap;
import edu.upenn.cis.cis455.ServiceFactory;
import edu.upenn.cis.cis455.WebServiceController;
import edu.upenn.cis.cis455.exceptions.HaltException;
import edu.upenn.cis.cis455.handlers.Route;
import edu.upenn.cis.cis455.m2.server.interfaces.Request;
import edu.upenn.cis.cis455.m2.server.interfaces.Response;

public class HttpRequestHandlerImplementation implements edu.upenn.cis.cis455.m1.server.interfaces.HttpRequestHandler {
	final static Logger logger= LogManager.getLogger(HttpRequestHandlerImplementation.class);
	  String  regex1 ="(\\\\w+/?)*";
	  String regex="/\\\\w+/";
	public Path serverRoot=null;
	public HttpRequestHandlerImplementation() {
		
		
	}

	public HttpRequestHandlerImplementation(Path serverRoot) {
		this.serverRoot = serverRoot;
		
	}
	

	public void handle(Request request, Response response) throws HaltException {
		
		RouteMap rm =WebServiceController.getMap();
		Route route=rm.get(request.requestMethod(), request.pathInfo());
		
		
		try {
			if(route!=null){
				Object obj=route.handle(request, response);
				if(obj!=null){
					response.body(obj.toString());
				}
				RequestImplementation request1=(RequestImplementation)request;
				String l=request1.sessionID;
				if(l !=null){
					//put Set-cookie in response 
					response.cookie(request.pathInfo(), "JSESSIONID", l, -1, true,true);
				}
				return;
			}
			else{
				//throw new HaltException(404, null,"Not Found");
				
				Set<String> KeySet=rm.keySet(request.requestMethod());
				
				Iterator<String> KeySetIter=KeySet.iterator();
				
				String pathInfo=request.pathInfo();
				
				
				while(KeySetIter.hasNext()){
					logger.debug("KeySetIter.hasNext()");
					
					String getRouteKey=KeySetIter.next();
					final String originalRouteKey= getRouteKey;
					getRouteKey=getRouteKey.replaceAll("/\\:\\w*/", regex);
					getRouteKey=getRouteKey.replaceAll("/\\:\\w*", "/(\\\\w+/?)");
					System.out.println("After replacing params   "+getRouteKey);
					String RouteKey=getRouteKey.replaceAll("/\\*/", regex);
					logger.debug(getRouteKey);
					
					RouteKey=RouteKey.replaceAll("\\*", regex1);
					
					if(request.pathInfo().matches(RouteKey)){
					 
						logger.debug("pathinfo matched the regex");
						
						route=rm.get(request.requestMethod(), originalRouteKey);
						
						Object body=route.handle(request, response);
						RequestImplementation request1=(RequestImplementation)request;
						String l=request1.sessionID;
						if(l !=null){
							//put Set-cookie in response 
							response.cookie(request.pathInfo(), "JSESSIONID", l, -1, true,true);
						}
						if(body!=null){
							response.body(body.toString());
						}
						return;
					}
				}
				WebServiceController.RequestValidation(request);
				handle2(request,response);
				
				//logger.debug("No Match");
				//throw new HaltException(404, null, "Not Found");
				
				
				
				
				
			}
		}
		catch (HaltException e) {
			// TODO Auto-generated catch block
			//HaltException x=(HaltException)e;
			throw e;
//			if(e!=null){
//				HaltException x = (HaltException) e;
//				throw x;
//			}
		}
		
		catch (Exception e) {
			// TODO Auto-generated catch block
			//HaltException x=(HaltException)e;
			e.printStackTrace();
//			if(e!=null){
//				HaltException x = (HaltException) e;
//				throw x;
//			}
		}
		
		
		

	}
	public void handle2(Request request, Response response) throws HaltException{
		InputStream fs=null;
		String root=WebServiceController.directory();
		String fileString=null;
		BufferedReader buf=null;
		RequestImplementation req=(RequestImplementation)request;
		StringBuilder sb=null;




		if(request.pathInfo().contentEquals("/")){
				int c;
				File file=null;
				try {
		
		
						file= new File(root+( request).pathInfo()+"index.html");
						fs= new FileInputStream(file);
						buf = new BufferedReader(new InputStreamReader(fs));
						sb = new StringBuilder();
		
				} 
				catch (FileNotFoundException e) {
						
						String body="<html><head><title>Fail!!</title></head><body><h1>HTTP/1.1</h1><p>file not found</p></body></html>";
						throw new HaltException(404,body);
		
				}
					String line=null;
					try {
							line = buf.readLine();
							while(line !=null){
								sb.append(line).append("\n");
								line = buf.readLine();
			
			
							}
						
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					fileString=sb.toString();
					response.contentLength((int)file.length());
					//String body="<html><head><title>Response</title></head><body><h1>Response</h1><p>test</p></body></html>";
					response.body(fileString);
					RequestImplementation request1=(RequestImplementation)request;
					String l=request1.sessionID;
					System.out.println("**************************************");

					logger.debug(fileString);
					response.bodyRaw(fileString.getBytes());
					response.status(200);
					response.type("text/html");
					if(l !=null){
						//put Set-cookie in response 
						System.out.println("bckjsanckjdsnckjnskjcnsakjcnsdkjnckdsnckjsdnkcnskjcns");
						response.cookie(request.pathInfo(), "JSESSIONID", l, -1, true,true);
						System.out.println("blah "+response);
					}
					try {
						fs.close();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
		

		}
		else if(request.pathInfo().contentEquals("/control")){
			
			
		}
		else if(request.pathInfo().contentEquals("/shutdown")){
			
			String body="<html><head><title>ShutDown</title></head><body><h1>Shutting the server down</h1><img src=\"https://media.tenor.com/images/ae1601098a0123ef9e17432476f8a416/tenor.gif\" width=\"244\" height=\"280\" style=\"background-color: rgb(255, 255, 255); width: 245px; height: 281.148px;\" alt=\"Yippee Ki Yay Motherfucker GIF - Diehard BruceWillis Yippeekiyay GIFs\"></body></html>";
			byte[] b=body.getBytes();
			response.bodyRaw(b);


			response.contentLength(b.length);
			response.status(200);
			response.type("text/html");
			
			
			
			
		}
		else{
			File file= new File(root+( request).pathInfo());
			logger.debug("***************"+root+( request).pathInfo());
			try {
				
				fs= new FileInputStream(root+( request).pathInfo());
			} catch (FileNotFoundException e) {
				String body="<html><head><title>Fail!!</title></head><body><h1>HTTP/1.1</h1><p>file not found</p></body></html>";
				HaltException x =new HaltException(404,body);
				throw x;



			}
			RequestImplementation request1=(RequestImplementation)request;
			byte[] b= new byte[(int)file.length()];


			int c=0;
			try {
				c=fs.read((byte[])b);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			response.bodyRaw(b);
			String l=request1.sessionID;
			System.out.println("**************************************");
			if(l !=null){
				//put Set-cookie in response 
				System.out.println("bckjsanckjdsnckjnskjcnsakjcnsdkjnckdsnckjsdnkcnskjcns");
				response.cookie(request.pathInfo(), "JSESSIONID", l, -1, true,true);
				System.out.println(response);
			}

			response.contentLength(c);
			response.status(200);
			response.type(request.contentType());

		}











		
		
		
		

	}




}
