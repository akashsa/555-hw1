package edu.upenn.cis.cis455.m1.server;

import java.net.Socket;
import java.util.Vector;
import java.util.concurrent.BlockingQueue;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Stub class for implementing the queue of HttpTasks
 */
public class HttpTaskQueue {
	
	final static Logger logger= LogManager.getLogger(HttpTaskQueue.class);
	
	Vector<HttpTask> TaskQueue;
	int requestQueueSize;
	
	HttpTaskQueue(int requestQueueSize){
		TaskQueue = new Vector<HttpTask>(requestQueueSize);
		this.requestQueueSize=requestQueueSize;
	}
	
	public boolean add(HttpTask H) throws InterruptedException{
		//TaskQueue.add(H);
		logger.info("[Output from log4j] Adding element to queue "+ Thread.currentThread().getName());//This would be logged in the log file created and to the console.
		//wait if the queue is full
		while (TaskQueue.size() == this.requestQueueSize) {
			// Synchronizing on the sharedQueue to make sure no more than one
			// thread is accessing the queue same time.
			logger.debug("Queue is full!");
			logger.debug("Queue is full!");
			synchronized (TaskQueue) {
				//
				//logger.info("Queue is full!");
				TaskQueue.wait();

			}
		}

		//Adding element to queue and notifying all waiting consumers
		synchronized (TaskQueue) {
			TaskQueue.add(H);
			TaskQueue.notifyAll();
		}
		

		
		
		return true;
	}
	public long  size(){
		
		
		
		return TaskQueue.size();
	}
	
	
//	public boolean add(HttpTask H) throws InterruptedException{
//		//TaskQueue.add(H);
//		logger.info("[Output from log4j] Adding element to queue "+ Thread.currentThread().getName());//This would be logged in the log file created and to the console.
//		//wait if the queue is full
//		
//			// Synchronizing on the sharedQueue to make sure no more than one
//			// thread is accessing the queue same time.
//			synchronized (TaskQueue) {
//				while (TaskQueue.size() == this.requestQueueSize) {
//				logger.debug("Queue is full!");
//				logger.info("Queue is full!");
//				TaskQueue.wait();
//
//			}
//				TaskQueue.add(H);
//				TaskQueue.notifyAll();
//			
//		}
//
//		//Adding element to queue and notifying all waiting consumers
//	
//
//		
//
//		
//		
//		return true;
//	}
	
	
	public HttpTask remove(int position) throws InterruptedException{
//		while (TaskQueue.isEmpty()) {
//			logger.debug("Queue is currently empty ");
//			//If the queue is empty, we push the current thread to waiting state. Way to avoid polling.
//			synchronized (TaskQueue) {
//				//
//				
//				//logger.debug("Putting thread to wait ");
//				
//				
//				TaskQueue.wait();
//			}
//		}
//
//		//Otherwise consume element and notify waiting producer
//		synchronized (TaskQueue) {
//			if (!TaskQueue.isEmpty()){
//				TaskQueue.notifyAll();
//				return TaskQueue.remove(position);
//			}
//		}
		while (true) {	// Or until a quit flag is set
			synchronized (TaskQueue) {
				if (!TaskQueue.isEmpty()) {
					TaskQueue.notifyAll();
					return TaskQueue.remove(0);
				} else
					TaskQueue.wait();
			}
		}
		
		
		
	}
//	public HttpTask remove(int position) throws InterruptedException{
//		
//			//If the queue is empty, we push the current thread to waiting state. Way to avoid polling.
//			synchronized (TaskQueue) {
//				while (TaskQueue.isEmpty()) {
//				logger.debug("Queue is currently empty ");
//				logger.info("Queue is currently empty!");
//				logger.debug("Putting thread to wait ");
//				logger.info("Putting thread to wait");
//				
//				TaskQueue.wait();
//			}
//		
//
//		//Otherwise consume element and notify waiting producer
//			
//			TaskQueue.notifyAll();
//			return TaskQueue.remove(position);
//					
//		}
//		
//	}	
	
	

}
