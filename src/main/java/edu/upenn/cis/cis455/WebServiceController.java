package edu.upenn.cis.cis455;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.TimeZone;

import javax.servlet.FilterRegistration;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import edu.upenn.cis.cis455.exceptions.HaltException;
import edu.upenn.cis.cis455.handlers.Filter;
import edu.upenn.cis.cis455.handlers.Route;
import edu.upenn.cis.cis455.m1.server.HttpServer;
import edu.upenn.cis.cis455.m1.server.interfaces.WebService;
import edu.upenn.cis.cis455.m2.server.interfaces.Request;
import edu.upenn.cis.cis455.m2.server.interfaces.Session;;

public  class WebServiceController {
	final static Logger logger= LogManager.getLogger(WebServiceController.class);
	static int port;
	public static String directory;
	static Path dir;

	static HttpServer basicServer;
	static int threadPoolSize = 10;
	static int requestQueueSize = 10;
	static HttpServiceController httpservicecontroller= (HttpServiceController) ServiceFactory.getServerInstance();
	
	
	
	
	public static void start() throws IOException {

//		basicServer=new HttpServer(threadPoolSize,requestQueueSize);
//		try {
//			
//			
//			basicServer.initializeHttpServer(port);
//			
//			
//			
//			
//		} catch (InterruptedException e) {
//			e.printStackTrace();
//		}
		

		
		httpservicecontroller.port(port);
		httpservicecontroller.staticFileLocation(directory);
		httpservicecontroller.awaitInitialization();
		
		
		
		
		
	}


	public static void stop() {
		// TODO Auto-generated method stub
		
	}


	public static void staticFileLocation(String directory1) {
		// TODO Auto-generated method stub
		directory=directory1;
		//dir=Paths.get(directory);
		
	}


	public static void get(String path, Route route) {
		// TODO Auto-generated method stub
	
		httpservicecontroller.routeMap.put("GET",path,route);
		

	}
	public static HashMap<String, Session> getSessionMap() {
		// TODO Auto-generated method stub
	
		return httpservicecontroller.SessionMap;
		

	}


	public static void ipAddress(String ipAddress) {
		// TODO Auto-generated method stub
		
		httpservicecontroller.ipAddress(ipAddress);

	}


	public static void port(int port1) {
		// TODO Auto-generated method stub
		port=port1;

	}


	public static void threadPool(int threads) {
		// TODO Auto-generated method stub
		threadPoolSize=threads;
		

	}
	public static void awaitInitialization() throws IOException {

		//registering filters


		start();
	}

	


	/**
	 * Triggers a HaltException that terminates the request
	 */
	public static HaltException halt() {
		throw new HaltException();
	}


	/**
	 * Triggers a HaltException that terminates the request
	 */
	public static HaltException halt(int statusCode) {
		throw new HaltException(statusCode);
	}

	/**
	 * Triggers a HaltException that terminates the request
	 */
	public static HaltException halt(String body) {
		throw new HaltException(body);
	}

	/**
	 * Triggers a HaltException that terminates the request
	 */
	public static HaltException halt(int statusCode, String body) {
		throw new HaltException(statusCode, body);
	}

	public static String directory()
	{
		return directory;
	}
	public static RouteMap getMap()
	{
		return httpservicecontroller.routeMap;
	}
	public static FilterStructure getBeforeStruct()
	{
		return httpservicecontroller.beforeStructure;
	}
	public static FilterStructure getAfterStruct()
	{
		return httpservicecontroller.afterStructure;
	}
	public static void post(String path, Route route) {
		// TODO Auto-generated method stub
		httpservicecontroller.put(path,route);
	}

	public static void put(String path, Route route) {
		// TODO Auto-generated method stub
		httpservicecontroller.put(path,route);
	}


	public static void delete(String path, Route route) {
		// TODO Auto-generated method stub
		httpservicecontroller.put(path,route);
	}


	public static void head(String path, Route route) {
		// TODO Auto-generated method stub
		httpservicecontroller.put(path,route);
	}

	
	public static void options(String path, Route route) {
		// TODO Auto-generated method stub
		httpservicecontroller.put(path,route);
	}

	
	public static void before(Filter filter) {
		// TODO Auto-generated method stub
		System.out.println("*************adding before filter**********");
		httpservicecontroller.before(filter);
		
	}

	
	public static void after(Filter filter) {
		// TODO Auto-generated method stub
		httpservicecontroller.after(filter);
	}

	
	public static void before(String path, String acceptType, Filter filter) {
		// TODO Auto-generated method stub
		httpservicecontroller.before(path,acceptType,filter);
	}

	
	public void after(String path, String acceptType, Filter filter) {
		// TODO Auto-generated method stub
		httpservicecontroller.afterStructure.put(path, acceptType, filter);
	}

	
public static void RequestValidation(Request request) throws HaltException{
		
		
		
		
		if(request.headers("host")==null){
			System.out.println("*********************<Host=null>*********************");
			HaltException e= new HaltException(400, null, "host cannot be empty");
			throw e;
		}
		
		String[] x=(request.pathInfo().split(request.headers("host")));
		
		logger.debug(Arrays.toString(x));
		//logger.debug("this "+(request.pathInfo().split("/")[1]));
		
		
		
		
		logger.debug(x.length);
		logger.debug(request.pathInfo());
		if(x.length>1){
			request.pathInfo(x[1]);
		}
		logger.debug(request.pathInfo());
		
		

		//Host header missing
		String protocolVersion= new String();
		//s=request.headers("host");
		
		String requestPathString= new String(WebServiceController.directory());
	
		requestPathString+=request.pathInfo();//request
		
		Path requestPath= Paths.get(requestPathString);
		
		requestPath=requestPath.toAbsolutePath().normalize();
		System.out.println(requestPath);
		String path=requestPath.toString();
		
		
		String rootString=new String( WebServiceController.directory());
		Path root= Paths.get(rootString);
		root=root.toAbsolutePath().normalize();
		
		System.out.println("root= "+root);
		System.out.println(WebServiceController.directory());
		
		System.out.println("path"+path);
		//Path dir2= Paths.get(rootdir2);
		System.out.println("**************"+path.startsWith(root.toString())+"********************");
		
		
		
		//check host == serveraddress and port
//		if(request.headers("host")!=null){
//			System.out.println(httpservicecontroller.ipAddress()+":"+httpservicecontroller.port());
//			
//			if(request.headers("host").equals(httpservicecontroller.ipAddress()+":"+httpservicecontroller.port())!=true){
//				HaltException e = new HaltException(400,null,"Bad dssrequest");
//				throw e;
//				
//			}
//		}
		
		
		
		
		
		protocolVersion=request.headers("protocolVersion");
		logger.debug(protocolVersion);
		 if(protocolVersion.equals("HTTP/1.1")!=true){
			System.out.println("*********************<protocolVersion.equals(\"HTTP/1.1\")!=true>*********************");
			// HTTP/1.0 throw code 505 version not supported
			HaltException e= new HaltException(505,null, "Worng Protocol");
			throw e;	
		}

		
		
		if(path.startsWith(root.toString())!=true){
			//absolute path does not exist
			System.out.println("*********************<Forbidden>*********************");
			HaltException e= new HaltException(403,null,"Forbidden");
			throw e;
			
		}
		logger.debug("request.headers(\"date\"):"+request.headers("date"));
		if(request.headers("date")!=null){
			//date in GMT
			checkDateFormat(request.headers("date"));
			
			
			
		}
		
		logger.debug("request.headers(\"If-Modified-Since\"):"+request.headers("if-modified-since"));
		if(request.headers("if-modified-since")!=null){
			
			System.out.println("*********************<If-Modified-Since>*********************");
			SimpleDateFormat sdf = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss z");
			sdf.setTimeZone(TimeZone.getTimeZone("G"));			
			String modifiedDate=request.headers("if-modified-since");
			logger.debug("request.headers(\"modified\"):"+request.headers("if-modified-since"));
			checkDateFormat(modifiedDate);
			Date parsedDate=null;
			Date date=null;
			Date fileDate=null;
			Date systemDate=new Date();
			
			try {
				parsedDate = sdf.parse(modifiedDate);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			String formattedStringDate=sdf.format(parsedDate);
			
			try {
				date= sdf.parse(formattedStringDate);
			} catch (ParseException e) {
				
				e.printStackTrace();
			}

			
			File file = new File(requestPathString);
			
			
			
			System.out.println("After Format : " + sdf.format(file.lastModified()));
			try {
				fileDate= sdf.parse((sdf.format(file.lastModified())));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			if(fileDate.compareTo(date)<=0){
				HaltException e= new HaltException(304, null, "Not Modified");
				throw e;
			}
			else if(date.compareTo(systemDate)>0){
				
				
				//sent date greater than system date. modified in future LOL
				HaltException e= new HaltException(400,null,"Bad request");
				e.getLocalizedMessage();
				throw e;
				
			}
			
			
		}
		logger.debug("request.headers(\"If-Unmodified-Since\"):"+request.headers("If-Unmodified-Since"));
		if(request.headers("If-Unmodified-Since")!=null){
			System.out.println("*********************<If-Unmodified-Since>*********************");
			
			SimpleDateFormat sdf = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss z");
			sdf.setTimeZone(TimeZone.getTimeZone("G"));
			String modifiedDate=request.headers("If-Unmodified-Since");
			checkDateFormat(modifiedDate);
			Date parsedDate=null;
			Date date=null;
			Date fileDate=null;
			Date systemDate=new Date();
			
			try {
				parsedDate = sdf.parse(modifiedDate);
				String formattedStringDate=sdf.format(parsedDate);
				
				date= sdf.parse(formattedStringDate);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			
			File file = new File(requestPathString);
			System.out.println("After Format : " + sdf.format(file.lastModified()));
			try {
				fileDate= sdf.parse((sdf.format(file.lastModified())));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			if(fileDate.compareTo(date)>0){
				HaltException e= new HaltException(412, null, "Precondition Failed");
				throw e;
			}
			else if(date.compareTo(systemDate)>0){
				
				
				//sent date greater than system date. modified in future LOL
				HaltException e= new HaltException(400,null,"Bad request");
				e.getLocalizedMessage();
				throw e;
				
			}
			
			
		}
		
	
		
		
		
		//400 bad request-- malformed request.
		
			
		
		
		
		
		
	}
	public static void checkDateFormat(String date1){
		//String date=request.headers("Date");
		logger.debug("******************<Checking Date>*****************");
		
		SimpleDateFormat sdf = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss z");
		sdf.setTimeZone(TimeZone.getTimeZone("G"));


		Date parsedDate=null;
		try {
			parsedDate= sdf.parse(date1);
			//logger.debug(parsedDate);
			
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//logger.debug(date1);

		logger.debug(sdf.format(parsedDate));
		if(date1.equals(sdf.format(parsedDate))){
			logger.debug(date1);
			logger.debug(sdf.format(parsedDate));
			return;
		}
		else{
			HaltException e= new HaltException(400,null,"Date format wrong");
			throw e;
		}
	}
	


}
