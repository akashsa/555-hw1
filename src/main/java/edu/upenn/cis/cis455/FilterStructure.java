package edu.upenn.cis.cis455;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Vector;

import edu.upenn.cis.cis455.handlers.Filter;

public class FilterStructure {
	
	
	public  Map<String, Map<String, Filter>>filterMap=null;
	public  Vector<Filter> filterVector= null;
	
	public FilterStructure() {
		filterMap = new HashMap<String, Map<String, Filter>>();
		filterMap.put("GET", new HashMap<String, Filter>());
		filterMap.put("HEAD", new HashMap<String, Filter>());
		filterMap.put("POST", new HashMap<String, Filter>());
		filterMap.put("DELETE", new HashMap<String, Filter>());
		filterMap.put("OPTIONS", new HashMap<String, Filter>());
		
		
		
		filterVector=new Vector<Filter>();
				// TODO Auto-generated constructor stub
	}
	public synchronized  void put(String routeMethod, String path, Filter filter){
		this.filterMap.get(routeMethod).put(path, filter);
		
		
	}
	public synchronized  Filter get(String acceptType, String path){
		return this.filterMap.get(acceptType).get(path);
		
		
	}
	public synchronized  void put(Filter filter){
		
		System.out.println("Adding element inside filter structure\n");
		
		Boolean lol=this.filterVector.add(filter);
		System.out.println(lol);
		System.out.println(filterVector.size());
		
	}
	public synchronized  Iterator<Filter> getIterator(){
		return this.filterVector.iterator();
		
		
	}
	
}
