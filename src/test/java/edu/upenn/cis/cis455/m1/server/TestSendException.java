package edu.upenn.cis.cis455.m1.server;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.TimeZone;
import static org.mockito.Mockito.*;

import edu.upenn.cis.cis455.ServiceFactory;
import edu.upenn.cis.cis455.WebServiceController;
import static edu.upenn.cis.cis455.WebServiceController.*;
import edu.upenn.cis.cis455.exceptions.HaltException;
import edu.upenn.cis.cis455.m1.server.HttpIoHandler;
import edu.upenn.cis.cis455.m1.server.implementations.HttpRequestHandlerImplementation;
import edu.upenn.cis.cis455.m1.server.implementations.RequestImplementation;
import edu.upenn.cis.cis455.m1.server.implementations.ResponseImplementation;
import edu.upenn.cis.cis455.m1.server.interfaces.HttpRequestHandler;
import edu.upenn.cis.cis455.m2.server.interfaces.Request;
import edu.upenn.cis.cis455.m2.server.interfaces.Response;
import edu.upenn.cis.cis455.m2.server.interfaces.Session;
import edu.upenn.cis.cis455.util.HttpParsing;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class TestSendException {

    final Logger logger = LogManager.getLogger(TestSendException.class);
    @Before    
    public void setUp() {
        //org.apache.logging.log4j.core.config.Configurator.setLevel("edu.upenn.cis.cis455", Level.DEBUG);
    	String root=System.getProperty("user.dir").toString()+"/www";
    	WebServiceController.staticFileLocation(root);
		get("/a/:name/test", (req,res)->{return null;});
		get("/a/*/test", (req,res)->{return null;});
		get("/a/*", (req,res)->{return null;});
		get("/a/:param1/c/:param2", (req,res)->{return null;});
    }
    
    String sampleGetRequest = 
        "GET /a/b/hello.htm?q=x&v=12%200 HTTP/1.1\r\n" +
        "User-Agent: Mozilla/4.0 (compatible; MSIE5.01; Windows NT)\r\n" +
        "Host: localhost:8080\r\n" +
        "Accept-Language: en-us\r\n" +
        "Accept-Encoding: gzip, deflate\r\n" +
        "Cookie: name1=value1;name2=value2;name3=value3\r\n" +
        "Content-Length: 3495\r\n"+
        "Connection: Keep-Alive\r\n\r\n";
    //@Test
    public void testpatternmatching() throws IOException {
		System.out.println("------------------------------------------------------------");
		System.out.println("--------- Test Cookie Invalid JSESSION ---------------------");
		System.out.println("------------------------------------------------------------");
        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        
        String sampleGetRequest = 
                "GET http://localhost:8080/ab/c/b/d HTTP/1.1\r\n" +
                "User-Agent: Mozilla/4.0 (compatible; MSIE5.01; Windows NT)\r\n" +
                "Host: localhost:8080\r\n" +
                "Accept-Language: en-us\r\n" +
                "Accept-Encoding: gzip, deflate\r\n" +
                "Cookie: JSESSION=asdasdasdsa;name2=value2;name3=value3\r\n" +
                "Content-Length: 3495\r\n"+
                "Connection: Keep-Alive\r\n\r\n"+"This is body man \r\n";
        
        Socket s = TestHelper.getMockSocket(
            sampleGetRequest, 
            byteArrayOutputStream);
        Map<String, String> headers = new HashMap<String,String>();
		Map<String, List<String>> parms = new HashMap<String, List<String>>();
		String uri = HttpParsing.parseRequest("0:0:0:0:0:0:0:1:00001", s.getInputStream(), headers, parms);
		 get("/ab/:param1/b/:param2", (reqt, resp)->{
			logger.debug("Its a match made in hell!!!!!!!!!!!!!!!!");
			return null;
		});
		Request request = ServiceFactory.createRequest(s, uri, false, headers, parms);
		Response response = ServiceFactory.createResponse();
		RequestImplementation new1= (RequestImplementation)request;

		System.out.println("this is the map"+new1.params());

		HttpRequestHandlerImplementation requestHandler= (HttpRequestHandlerImplementation) ServiceFactory.createRequestHandlerInstance(null);
		requestHandler.handle(request, response);
		String x = new String(new1.body());
		//System.out.println(x);
		logger.debug("body in test=",x);
		Session session = request.session();
		logger.debug("sdfjsdljflksj"+request.cookie("name2"));
		assertTrue(session == null);
		session = request.session(true);
		assertTrue(session != null);
		System.out.println("Generated Session ID: " + session.id());
    }
   
    
 // @Test
  public void testCookie() throws IOException {
	  ResponseImplementation rs = new ResponseImplementation();
	  rs.cookie("/", "JsessionID", "xasx", 10, true, true);
	  rs.cookie("/", "JsessionID", "xasx");
	  rs.cookie("/", "JSessionID", "xasx", 10, true, true);
	  rs.type("text/HTML");
	  System.out.println(rs.getHeaders());
      //assertTrue(result.startsWith("HTTP/1.1 404"));
      //assertTrue(true);
      //new comment to commit
	  getSessionMap();
      
  }
  
  @Test
  public void testSession1() throws IOException {

	  String id= ServiceFactory.createSession();
	 System.out.println( getSessionMap().size());
	 
	  assertTrue(( getSessionMap().size()) ==1);
	  assertTrue( getSessionMap().get(id)!=null);
	  Session x = getSessionMap().get(id);
	  x.attribute("User", "USER");
	  Session x2 = getSessionMap().get(id);
      assertTrue(x2.attribute("User").equals("USER"));
      x2.invalidate();
      Session x3 = getSessionMap().get(id);
      assertTrue(x3==null);
  }
    

	@Test
  public void testNoCookie() throws IOException {
		System.out.println("------------------------------------------------------------");
		System.out.println("--------- Test No Cookie  ----------------------------------");
		System.out.println("------------------------------------------------------------");
      final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
      
      String sampleGetRequest = 
              "GET http://localhost:8080/index.html HTTP/1.1\r\n" +
              "User-Agent: Mozilla/4.0 (compatible; MSIE5.01; Windows NT)\r\n" +
              "Host: localhost:8080\r\n" +
              "Accept-Language: en-us\r\n" +
              "Accept-Encoding: gzip, deflate\r\n" +
              "Connection: Keep-Alive\r\n\r\n";
      
      Socket s = TestHelper.getMockSocket(
          sampleGetRequest, 
          byteArrayOutputStream);
      Map<String, String> headers = new HashMap<String,String>();
		Map<String, List<String>> parms = new HashMap<String, List<String>>();
		String uri = HttpParsing.parseRequest("0:0:0:0:0:0:0:1:00001", s.getInputStream(), headers, parms);
		Request request = ServiceFactory.createRequest(s, uri, false, headers, parms);
		Session session = request.session();
		assertTrue(session == null);
		session = request.session(true);
		assertTrue(session != null);
		System.out.println("Generated Session ID: " + session.id());
  }
	
	@Test
  public void testCookieInvalidJSESSION() throws IOException {
		System.out.println("------------------------------------------------------------");
		System.out.println("--------- Test Cookie Invalid JSESSION ---------------------");
		System.out.println("------------------------------------------------------------");
      final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
      
      String sampleGetRequest = 
              "GET http://localhost:8080/index.html HTTP/1.1\r\n" +
              "User-Agent: Mozilla/4.0 (compatible; MSIE5.01; Windows NT)\r\n" +
              "Host: localhost:8080\r\n" +
              "Accept-Language: en-us\r\n" +
              "Accept-Encoding: gzip, deflate\r\n" +
              "Cookie: JSESSION=asdasdasdsa;name2=value2;name3=value3\r\n" +
              "Connection: Keep-Alive\r\n\r\n";
      
      Socket s = TestHelper.getMockSocket(
          sampleGetRequest, 
          byteArrayOutputStream);
      Map<String, String> headers = new HashMap<String,String>();
		Map<String, List<String>> parms = new HashMap<String, List<String>>();
		String uri = HttpParsing.parseRequest("0:0:0:0:0:0:0:1:00001", s.getInputStream(), headers, parms);
		Request request = ServiceFactory.createRequest(s, uri, false, headers, parms);
		Session session = request.session();
		assertTrue(session == null);
		session = request.session(true);
		assertTrue(session != null);
		System.out.println("Generated Session ID: " + session.id());
  }
	
	
	
	@Test
  public void testSession() throws IOException {
		System.out.println("------------------------------------------------------------");
		System.out.println("--------- Test Session  ------------------------------------");
		System.out.println("------------------------------------------------------------");
      final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
      
      String sampleGetRequest = 
              "GET http://localhost:8080/index.html HTTP/1.1\r\n" +
              "User-Agent: Mozilla/4.0 (compatible; MSIE5.01; Windows NT)\r\n" +
              "Host: localhost:8080\r\n" +
              "Accept-Language: en-us\r\n" +
              "Accept-Encoding: gzip, deflate\r\n" +
              "Cookie: name1=value1;name2=value2;name3=value3\r\n" +
              "Connection: Keep-Alive\r\n\r\n";
      
      Socket s = TestHelper.getMockSocket(
          sampleGetRequest, 
          byteArrayOutputStream);
      Map<String, String> headers = new HashMap<String,String>();
		Map<String, List<String>> parms = new HashMap<String, List<String>>();
		String uri = HttpParsing.parseRequest("0:0:0:0:0:0:0:1:00001", s.getInputStream(), headers, parms);
		Request request = ServiceFactory.createRequest(s, uri, false, headers, parms);
		Session session = request.session();
		assertTrue(session == null);
		session = request.session(true);
		assertTrue(session != null);
		System.out.println("Generated Session ID: " + session.id());
  }
	
	@Test
  public void testInvalidJESSION() throws IOException {
		System.out.println("------------------------------------------------------------");
		System.out.println("--------- Test Invalid JESSION  ----------------------------");
		System.out.println("------------------------------------------------------------");
      final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
      
      String sampleGetRequest = 
              "GET http://localhost:8080/index.html HTTP/1.1\r\n" +
              "User-Agent: Mozilla/4.0 (compatible; MSIE5.01; Windows NT)\r\n" +
              "Host: localhost:8080\r\n" +
              "Accept-Language: en-us\r\n" +
              "Accept-Encoding: gzip, deflate\r\n" +
              "Cookie: name1=value1;name2=value2;name3=value3\r\n" +
              "Connection: Keep-Alive\r\n\r\n";
      
      Socket s = TestHelper.getMockSocket(
          sampleGetRequest, 
          byteArrayOutputStream);
      Map<String, String> headers = new HashMap<String,String>();
		Map<String, List<String>> parms = new HashMap<String, List<String>>();
		String uri = HttpParsing.parseRequest("0:0:0:0:0:0:0:1:00001", s.getInputStream(), headers, parms);
		Request request = ServiceFactory.createRequest(s, uri, false, headers, parms);
		Session session = request.session(true);
		System.out.println("Generated Session ID: " + session.id());
		assertTrue(session != null);
		 String newSampleGetRequest = 
	                "GET http://localhost:8080/index.html HTTP/1.1\r\n" +
	                "User-Agent: Mozilla/4.0 (compatible; MSIE5.01; Windows NT)\r\n" +
	                "Host: localhost:8080\r\n" +
	                "Accept-Language: en-us\r\n" +
	                "Accept-Encoding: gzip, deflate\r\n" +
	                "Cookie: JSESSIONID=" + session.id() + "aasd;name2=value2;name3=value3\r\n" +
	                "Connection: Keep-Alive\r\n\r\n";
		 
		 s = TestHelper.getMockSocket(
				 	newSampleGetRequest, 
		            byteArrayOutputStream);
		 uri = HttpParsing.parseRequest("0:0:0:0:0:0:0:1:00001", s.getInputStream(), headers, parms);
		 request = ServiceFactory.createRequest(s, uri, false, headers, parms);
		 session = request.session();
		 assertTrue(session == null);
  }
	
	@Test
  public void testValidSession() throws IOException {
		System.out.println("------------------------------------------------------------");
		System.out.println("--------- Test Valid Session  ------------------------------");
		System.out.println("------------------------------------------------------------");
      final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
      
      String sampleGetRequest = 
              "GET http://localhost:8080/index.html HTTP/1.1\r\n" +
              "User-Agent: Mozilla/4.0 (compatible; MSIE5.01; Windows NT)\r\n" +
              "Host: localhost:8080\r\n" +
              "Accept-Language: en-us\r\n" +
              "Accept-Encoding: gzip, deflate\r\n" +
              "Cookie: name1=value1;name2=value2;name3=value3\r\n" +
              "Connection: Keep-Alive\r\n\r\n";
      
      Socket s = TestHelper.getMockSocket(
          sampleGetRequest, 
          byteArrayOutputStream);
      Map<String, String> headers = new HashMap<String,String>();
		Map<String, List<String>> parms = new HashMap<String, List<String>>();
		String uri = HttpParsing.parseRequest("0:0:0:0:0:0:0:1:00001", s.getInputStream(), headers, parms);
		Request request = ServiceFactory.createRequest(s, uri, false, headers, parms);
		Session session = request.session(true);
		String generatedSessionID = session.id();
		System.out.println("Generated Session ID: " + session.id());
		assertTrue(session != null);
		 String newSampleGetRequest = 
	                "GET http://localhost:8080/index.html HTTP/1.1\r\n" +
	                "User-Agent: Mozilla/4.0 (compatible; MSIE5.01; Windows NT)\r\n" +
	                "Host: localhost:8080\r\n" +
	                "Accept-Language: en-us\r\n" +
	                "Accept-Encoding: gzip, deflate\r\n" +
	                "Cookie: JSESSIONID=" + session.id() + ";name2=value2;name3=value3\r\n" +
	                "Connection: Keep-Alive\r\n\r\n";
		 
		 s = TestHelper.getMockSocket(
				 	newSampleGetRequest, 
		            byteArrayOutputStream);
		 uri = HttpParsing.parseRequest("0:0:0:0:0:0:0:1:00001", s.getInputStream(), headers, parms);
		 request = ServiceFactory.createRequest(s, uri, false, headers, parms);
		 session = request.session();
		 assertTrue(session != null);
		 String receivedSessionID = session.id();
		 System.out.println("Received Session  ID: " + session.id());
		 assertTrue(generatedSessionID.equals(receivedSessionID));
  }
	
	@Test
	public void testJESSIONID_IN_Response() throws IOException{
		System.out.println("------------------------------------------------------------");
		System.out.println("--------- Testing JSESSIONID In Response  ------------------");
		System.out.println("------------------------------------------------------------");
		String sampleGetRequest = 
				"GET / HTTP/1.1\r\n" +
						"Host: localhost:8080\r\n" +
						"User-Agent: Mozilla/4.0 (compatible; MSIE5.01; Windows NT)\r\n" +
						"Accept-Language: en-us\r\n" +
						"Accept-Encoding: gzip, deflate\r\n" +
						"Cookie: name1=value1;name2=value2;name3=value3\r\n" +
						"Connection: Keep-Alive\r\n\r\n";

		HashMap<String, String> headers = new HashMap<String, String>();
		HashMap<String, List<String>> parms = new HashMap<String, List<String>>();
		String rootDirectory = "www";
		final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
		Socket s = TestHelper.getMockSocket(
				sampleGetRequest, 
				byteArrayOutputStream);
		String uri = HttpParsing.parseRequest("0:0:0:0:0:0:0:1:00001", s.getInputStream(), headers, parms);
		
		HttpRequestHandler requestHandler = ServiceFactory.createRequestHandlerInstance(null);
		Request request = ServiceFactory.createRequest(s, uri, false, headers, parms);
		Response response = ServiceFactory.createResponse();
		Session session = request.session(true);
		assertTrue(session != null);
		String generatedSession = session.id();
		response.cookie("JSESSIONID", generatedSession);
		requestHandler.handle(request, response);
		HttpIoHandler.sendResponse(s, request, response);
		String result = byteArrayOutputStream.toString("UTF-8").replace("\r", "");
		assertTrue(result.contains("Set-Cookie: JSESSIONID=" + generatedSession));
	}
	
	@Test
	public void testJESSIONID_NOT_IN_Response() throws IOException{
		System.out.println("------------------------------------------------------------");
		System.out.println("--------- Testing NO JSESSIONID In Response  ---------------");
		System.out.println("------------------------------------------------------------");
		String sampleGetRequest = 
				"GET / HTTP/1.1\r\n" +
						"Host: localhost:8080\r\n" +
						"User-Agent: Mozilla/4.0 (compatible; MSIE5.01; Windows NT)\r\n" +
						"Accept-Language: en-us\r\n" +
						"Accept-Encoding: gzip, deflate\r\n" +
						"Cookie: name1=value1;name2=value2;name3=value3\r\n" +
						"Connection: Keep-Alive\r\n\r\n";

		HashMap<String, String> headers = new HashMap<String, String>();
		HashMap<String, List<String>> parms = new HashMap<String, List<String>>();
		String rootDirectory = "www";
		final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
		Socket s = TestHelper.getMockSocket(
				sampleGetRequest, 
				byteArrayOutputStream);
		String uri = HttpParsing.parseRequest("0:0:0:0:0:0:0:1:00001", s.getInputStream(), headers, parms);
		
		HttpRequestHandler requestHandler = ServiceFactory.createRequestHandlerInstance(null);
		Request request = ServiceFactory.createRequest(s, uri, false, headers, parms);
		Response response = ServiceFactory.createResponse();
		Session session = request.session(false);
		assertTrue(session == null);
		requestHandler.handle(request, response);
		HttpIoHandler.sendResponse(s, request, response);
		String result = byteArrayOutputStream.toString("UTF-8").replace("\r", "");
		assertTrue(!result.contains("Set-Cookie: JSESSIONID="));
	}
	
	@Test
	public void testJESSIONID_EXCHANGE() throws IOException{
		System.out.println("------------------------------------------------------------");
		System.out.println("--------- Testing JSESSIONID Exchange  ---------------------");
		System.out.println("------------------------------------------------------------");
		String sampleGetRequest = 
				"GET / HTTP/1.1\r\n" +
						"Host: localhost:8080\r\n" +
						"User-Agent: Mozilla/4.0 (compatible; MSIE5.01; Windows NT)\r\n" +
						"Accept-Language: en-us\r\n" +
						"Accept-Encoding: gzip, deflate\r\n" +
						"Connection: Keep-Alive\r\n\r\n";

		HashMap<String, String> headers = new HashMap<String, String>();
		HashMap<String, List<String>> parms = new HashMap<String, List<String>>();
		String rootDirectory = "www";
		final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
		Socket s = TestHelper.getMockSocket(
				sampleGetRequest, 
				byteArrayOutputStream);
		HttpRequestHandler requestHandler = ServiceFactory.createRequestHandlerInstance(null);
		
		String uri = HttpParsing.parseRequest("0:0:0:0:0:0:0:1:00001", s.getInputStream(), headers, parms);
		Request request = ServiceFactory.createRequest(s, uri, false, headers, parms);
		Response response = ServiceFactory.createResponse();
		Session session = request.session(true);
		assertTrue(session != null);
		String generatedSession = session.id();
		response.cookie("JSESSIONID", generatedSession);
		requestHandler.handle(request, response);
		HttpIoHandler.sendResponse(s, request, response);
		String result = byteArrayOutputStream.toString("UTF-8").replace("\r", "");
		assertTrue(result.contains("Set-Cookie: JSESSIONID=" + generatedSession));
		String newSampleGetRequest = 
              "GET http://localhost:8080/index.html HTTP/1.1\r\n" +
              "User-Agent: Mozilla/4.0 (compatible; MSIE5.01; Windows NT)\r\n" +
              "Host: localhost:8080\r\n" +
              "Accept-Language: en-us\r\n" +
              "Accept-Encoding: gzip, deflate\r\n" +
              "Cookie: JSESSIONID=" + generatedSession + ";name2=value2;name3=value3\r\n" +
              "Connection: Keep-Alive\r\n\r\n";
		final ByteArrayOutputStream byteArrayOutputStream1 = new ByteArrayOutputStream();
		Socket s1 = TestHelper.getMockSocket(
				 	newSampleGetRequest, 
				 	byteArrayOutputStream1);
		 uri = HttpParsing.parseRequest("0:0:0:0:0:0:0:1:00001", s1.getInputStream(), headers, parms);
		 request = ServiceFactory.createRequest(s, uri, false, headers, parms);
		 response = ServiceFactory.createResponse();
		 session = request.session();
		 assertTrue(session != null);
		 String receivedSessionID = session.id();
		 assertTrue(generatedSession.equals(receivedSessionID));
		 requestHandler.handle(request, response);
			HttpIoHandler.sendResponse(s, request, response);
		 String result_2 = byteArrayOutputStream1.toString("UTF-8").replace("\r", "");
		 assertTrue(!result_2.contains("Set-Cookie: JSESSIONID="));
		 
	} 

	@Test
	public void testWildCard_A() throws IOException{
		System.out.println("------------------------------------------------------------");
		System.out.println("--------- Test Wild Card Variation A -----------------------");
		System.out.println("------------------------------------------------------------");
		String sampleGetRequest = 
				"GET /a/b/test HTTP/1.1\r\n" +
						"Host: www.cis.upenn.edu\r\n" +
						"User-Agent: Mozilla/4.0 (compatible; MSIE5.01; Windows NT)\r\n" +
						"Accept-Language: en-us\r\n" +
						"Accept-Encoding: gzip, deflate\r\n" +
						"Connection: Keep-Alive\r\n\r\n";

		HashMap<String, String> headers = new HashMap<String, String>();
		HashMap<String, List<String>> parms = new HashMap<String, List<String>>();
		String rootDirectory = "www";
		final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
		Socket s = TestHelper.getMockSocket(
				sampleGetRequest, 
				byteArrayOutputStream);
		HttpRequestHandler requestHandler = ServiceFactory.createRequestHandlerInstance(null);

		String uri = HttpParsing.parseRequest("0:0:0:0:0:0:0:1:00001", s.getInputStream(), headers, parms);
		Request request = ServiceFactory.createRequest(s, uri, false, headers, parms);
		Response response = ServiceFactory.createResponse();
		
		requestHandler.handle(request, response);
		HttpIoHandler.sendResponse(s, request, response);
		String result = byteArrayOutputStream.toString("UTF-8").replace("\r", "");
		assertTrue(result.startsWith("HTTP/1.1 200"));

	}
	
	@Test
	public void testWildCard_B() throws IOException{
		System.out.println("------------------------------------------------------------");
		System.out.println("--------- Test Wild Card Variation B -----------------------");
		System.out.println("------------------------------------------------------------");
		String sampleGetRequest = 
				"GET /a/b/b/test HTTP/1.1\r\n" +
						"Host: www.cis.upenn.edu\r\n" +
						"User-Agent: Mozilla/4.0 (compatible; MSIE5.01; Windows NT)\r\n" +
						"Accept-Language: en-us\r\n" +
						"Accept-Encoding: gzip, deflate\r\n" +
						"Connection: Keep-Alive\r\n\r\n";

		HashMap<String, String> headers = new HashMap<String, String>();
		HashMap<String, List<String>> parms = new HashMap<String, List<String>>();
		final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
		Socket s = TestHelper.getMockSocket(
				sampleGetRequest, 
				byteArrayOutputStream);
		HttpRequestHandler requestHandler = ServiceFactory.createRequestHandlerInstance(null);

		String uri = HttpParsing.parseRequest("0:0:0:0:0:0:0:1:00001", s.getInputStream(), headers, parms);
		Request request = ServiceFactory.createRequest(s, uri, false, headers, parms);
		Response response = ServiceFactory.createResponse();
		try{
			requestHandler.handle(request, response);
		}catch(HaltException e){
			assertTrue(e.statusCode()==404);
		}
//		HttpIoHandler.sendResponse(s, request, response);
//		String result = byteArrayOutputStream.toString("UTF-8").replace("\r", "");
		//assertTrue(result.startsWith("HTTP/1.1 200"));

	}
	
	@Test
	public void testWildCard_C() throws IOException{
		System.out.println("------------------------------------------------------------");
		System.out.println("--------- Test Wild Card Variation C -----------------------");
		System.out.println("------------------------------------------------------------");
		String sampleGetRequest = 
				"GET /a/test HTTP/1.1\r\n" +
						"Host: www.cis.upenn.edu\r\n" +
						"User-Agent: Mozilla/4.0 (compatible; MSIE5.01; Windows NT)\r\n" +
						"Accept-Language: en-us\r\n" +
						"Accept-Encoding: gzip, deflate\r\n" +
						"Connection: Keep-Alive\r\n\r\n";

		HashMap<String, String> headers = new HashMap<String, String>();
		HashMap<String, List<String>> parms = new HashMap<String, List<String>>();
		final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
		Socket s = TestHelper.getMockSocket(
				sampleGetRequest, 
				byteArrayOutputStream);
		HttpRequestHandler requestHandler = ServiceFactory.createRequestHandlerInstance(null);

		String uri = HttpParsing.parseRequest("0:0:0:0:0:0:0:1:00001", s.getInputStream(), headers, parms);
		Request request = ServiceFactory.createRequest(s, uri, false, headers, parms);
		Response response = ServiceFactory.createResponse();
		
		requestHandler.handle(request, response);
		HttpIoHandler.sendResponse(s, request, response);
		String result = byteArrayOutputStream.toString("UTF-8").replace("\r", "");
		assertTrue(result.startsWith("HTTP/1.1 200"));

	}
	
	@Test
	public void testWildCard_D() throws IOException{
		System.out.println("------------------------------------------------------------");
		System.out.println("--------- Test Wild Card Variation D -----------------------");
		System.out.println("------------------------------------------------------------");
		String sampleGetRequest = 
				"GET /a/test/c/test HTTP/1.1\r\n" +
						"Host: www.cis.upenn.edu\r\n" +
						"User-Agent: Mozilla/4.0 (compatible; MSIE5.01; Windows NT)\r\n" +
						"Accept-Language: en-us\r\n" +
						"Accept-Encoding: gzip, deflate\r\n" +
						"Connection: Keep-Alive\r\n\r\n";

		HashMap<String, String> headers = new HashMap<String, String>();
		HashMap<String, List<String>> parms = new HashMap<String, List<String>>();
		final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
		Socket s = TestHelper.getMockSocket(
				sampleGetRequest, 
				byteArrayOutputStream);
		HttpRequestHandler requestHandler = ServiceFactory.createRequestHandlerInstance(null);

		String uri = HttpParsing.parseRequest("0:0:0:0:0:0:0:1:00001", s.getInputStream(), headers, parms);
		Request request = ServiceFactory.createRequest(s, uri, false, headers, parms);
		Response response = ServiceFactory.createResponse();
		
		requestHandler.handle(request, response);
		HttpIoHandler.sendResponse(s, request, response);
		String result = byteArrayOutputStream.toString("UTF-8").replace("\r", "");
		assertTrue(result.startsWith("HTTP/1.1 200"));

	}
	
	@Test
	public void testWildCard_E() throws IOException{
		System.out.println("------------------------------------------------------------");
		System.out.println("--------- Test Wild Card Variation E -----------------------");
		System.out.println("------------------------------------------------------------");
		String sampleGetRequest = 
				"GET /a/test/c HTTP/1.1\r\n" +
						"Host: www.cis.upenn.edu\r\n" +
						"User-Agent: Mozilla/4.0 (compatible; MSIE5.01; Windows NT)\r\n" +
						"Accept-Language: en-us\r\n" +
						"Accept-Encoding: gzip, deflate\r\n" +
						"Connection: Keep-Alive\r\n\r\n";

		HashMap<String, String> headers = new HashMap<String, String>();
		HashMap<String, List<String>> parms = new HashMap<String, List<String>>();
		final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
		Socket s = TestHelper.getMockSocket(
				sampleGetRequest, 
				byteArrayOutputStream);
		HttpRequestHandler requestHandler = ServiceFactory.createRequestHandlerInstance(null);

		String uri = HttpParsing.parseRequest("0:0:0:0:0:0:0:1:00001", s.getInputStream(), headers, parms);
		Request request = ServiceFactory.createRequest(s, uri, false, headers, parms);
		Response response = ServiceFactory.createResponse();
		
		requestHandler.handle(request, response);
		HttpIoHandler.sendResponse(s, request, response);
		String result = byteArrayOutputStream.toString("UTF-8").replace("\r", "");
		assertTrue(result.startsWith("HTTP/1.1 200"));
	}
	

//    @Test
//    public void testSendException() throws IOException {
//    	final Logger logger = LogManager.getLogger(TestSendException.class);
//        logger.debug("***********************<testSendException()>************************");
//        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
//        Socket s = TestHelper.getMockSocket(
//            sampleGetRequest, 
//            byteArrayOutputStream);
//
//        HaltException halt = new HaltException(404, "Not found");
//        
//        HttpIoHandler.sendException(s, null, halt);
//        String result = byteArrayOutputStream.toString("UTF-8").replace("\r", "");
//        System.out.println(result);
//        
//        assertTrue(result.startsWith("HTTP/1.1 404"));
//        //assertTrue(true);
//        //new comment to commit
//        
//        
//    }
//    @Test
//    public void testSecurity() throws IOException {
//    	final Logger logger = LogManager.getLogger(TestSendException.class);
//        logger.debug("***********************<testSecurity()>************************");
//        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
//        
//        String sampleGetRequest = 
//                "GET /../../src/main/java/edu/upenn/cis/cis455/WebServer.java" +"\r\n"+
//                "User-Agent: Mozilla/4.0 (compatible; MSIE5.01; Windows NT)\r\n" +
//                "Date: Tue, 26 Sep 2017 01:38:13 GMT"+"\r\n"+
//                "protocolVersion: HTTP/1.1"+"\r\n"+
//                "Expect: 100 Continue\r\n"+
//                "Host: localhost:9050\r\n" +
//                "Accept-Language: en-us\r\n" +
//                "Accept-Encoding: gzip, deflate\r\n" +
//                "Cookie: name1=value1;name2=value2;name3=value3\r\n" +
//                "Connection: Keep-Alive\r\n\r\n";
//        Socket s = TestHelper.getMockSocket(
//        		sampleGetRequest, 
//            byteArrayOutputStream);
//        
//       
// 
//         staticFileLocation("/projects/555-hw1/www");
//        HttpServer basicServer=new HttpServer(10,10,"/projects/555-hw1/www", true, s);
//        try {
//			basicServer.initializeHttpServer(9050);
//		} catch (InterruptedException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//        String result=new String("");
//        while(result.equals("")){
//        	result = byteArrayOutputStream.toString("UTF-8").replace("\r", "");
//        	if(result.startsWith("HTTP/1.1 100")){
//        		byteArrayOutputStream.reset();
//        		logger.debug(result);
//
//        		result="";
//        		
//        	}
//        }
//        
//        System.out.println("The result is"+result);
// 
//        assertTrue(result.startsWith("HTTP/1.1 403"));
//
//        
//    }
//  @Test
//    public void testURLParsing() throws IOException {
//    	final Logger logger = LogManager.getLogger(TestSendException.class);
//        logger.debug("***********************<testSecurity()>************************");
//        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
//        
//        String sampleGetRequest = 
//                "GET /path?test1=param1" +"\r\n"+
//                "User-Agent: Mozilla/4.0 (compatible; MSIE5.01; Windows NT)\r\n" +
//                "Date: Tue, 26 Sep 2017 01:38:13 GMT"+"\r\n"+
//                "protocolVersion: HTTP/1.1"+"\r\n"+
//                "Expect: 100 Continue\r\n"+
//                "Host: my.url\r\n" +
//                "Accept-Language: en-us\r\n" +
//                "Accept-Encoding: gzip, deflate\r\n" +
//                "Cookie: name1=value1;name2=value2;name3=value3\r\n" +
//                "Connection: Keep-Alive\r\n\r\n";
//        Socket s = TestHelper.getMockSocket(
//        		sampleGetRequest, 
//            byteArrayOutputStream);
//        
//       
//        
//         staticFileLocation("/projects/555-hw1/www");
//        Map<String, String> headers = new HashMap<String,String>();
//		Map<String, List<String>> parms = new HashMap<String, List<String>>();
//		String uri = HttpParsing.parseRequest("0:0:0:0:0:0:0:1:65301", s.getInputStream(), headers, parms);
//		
//		Request request = ServiceFactory.createRequest(s, uri, false, headers, parms);
//        
//        System.out.println("URL is"+request.url()+"   "+request.uri());
// 
//        assertTrue(request.url().contentEquals("http://my.url/path?test1=param1"));
//        assertTrue(request.uri().contentEquals("http://my.url/path"));
//
//        
//    }
//  @Test
//  public void testURLParsinghttp() throws IOException {
//  	final Logger logger = LogManager.getLogger(TestSendException.class);
//      logger.debug("***********************<testSecurity()>************************");
//      final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
//      
//      String sampleGetRequest = 
//              "GET http://my.url/path?test1=param1" +"\r\n"+
//              "User-Agent: Mozilla/4.0 (compatible; MSIE5.01; Windows NT)\r\n" +
//              "Date: Tue, 26 Sep 2017 01:38:13 GMT"+"\r\n"+
//              "protocolVersion: HTTP/1.1"+"\r\n"+
//              "Expect: 100 Continue\r\n"+
//              "Host: my.url\r\n" +
//              "Accept-Language: en-us\r\n" +
//              "Accept-Encoding: gzip, deflate\r\n" +
//              "Cookie: name1=value1;name2=value2;name3=value3\r\n" +
//              "Connection: Keep-Alive\r\n\r\n";
//      Socket s = TestHelper.getMockSocket(
//      		sampleGetRequest, 
//          byteArrayOutputStream);
//      
//     
//      
//       staticFileLocation("/projects/555-hw1/www");
//      Map<String, String> headers = new HashMap<String,String>();
//		Map<String, List<String>> parms = new HashMap<String, List<String>>();
//		String uri = HttpParsing.parseRequest("0:0:0:0:0:0:0:1:65301", s.getInputStream(), headers, parms);
//		
//		Request request = ServiceFactory.createRequest(s, uri, false, headers, parms);
//      
//      System.out.println("URL is"+request.url()+"   "+request.uri());
//
//      assertTrue(request.url().contentEquals("http://my.url/path?test1=param1"));
//      assertTrue(request.uri().contentEquals("http://my.url/path"));
//
//      
//  }
//  @Test
//  public void testURLParsingpathInfo() throws IOException {
//  	final Logger logger = LogManager.getLogger(TestSendException.class);
//      logger.debug("***********************<testSecurity()>************************");
//      final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
//      
//      String sampleGetRequest = 
//              "GET http://my.url/path?test1=param1" +"\r\n"+
//              "User-Agent: Mozilla/4.0 (compatible; MSIE5.01; Windows NT)\r\n" +
//              "Date: Tue, 26 Sep 2017 01:38:13 GMT"+"\r\n"+
//              "protocolVersion: HTTP/1.1"+"\r\n"+
//              "Expect: 100 Continue\r\n"+
//              "Host: my.url\r\n" +
//              "Accept-Language: en-us\r\n" +
//              "Accept-Encoding: gzip, deflate\r\n" +
//              "Cookie: name1=value1;name2=value2;name3=value3\r\n" +
//              "Connection: Keep-Alive\r\n\r\n";
//      Socket s = TestHelper.getMockSocket(
//      		sampleGetRequest, 
//          byteArrayOutputStream);
//      
//     
//      
//       staticFileLocation("/projects/555-hw1/www");
//      Map<String, String> headers = new HashMap<String,String>();
//		Map<String, List<String>> parms = new HashMap<String, List<String>>();
//		String uri = HttpParsing.parseRequest("0:0:0:0:0:0:0:1:65301", s.getInputStream(), headers, parms);
//		
//		Request request = ServiceFactory.createRequest(s, uri, false, headers, parms);
//      
//      System.out.println("URL is"+request.url()+"   "+request.uri());
//
//      assertTrue(request.url().contentEquals("http://my.url/path?test1=param1"));
//      assertTrue(request.uri().contentEquals("http://my.url/path"));
//
//      
//  }
//    @Test
//    public void testMissingHost() throws IOException {
//    	final Logger logger = LogManager.getLogger(TestSendException.class);
//        logger.debug("***********************<testMissingHost()>************************");
//        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
//       
//        String sampleGetRequest = 
//                "GET /" +"\r\n"+
//                "User-Agent: Mozilla/4.0 (compatible; MSIE5.01; Windows NT)\r\n" +
//                "Date: Tue, 26 Sep 2017 01:38:13 GMT"+"\r\n"+
//                "protocolVersion: HTTP/1.1"+"\r\n"+
//                "Accept-Language: en-us\r\n" +
//                "Accept-Encoding: gzip, deflate\r\n" +
//                "Cookie: name1=value1;name2=value2;name3=value3\r\n" +
//                "Connection: Keep-Alive\r\n\r\n";
//        Socket s = TestHelper.getMockSocket(
//        		sampleGetRequest, 
//            byteArrayOutputStream);
//        
// 
//         staticFileLocation("/projects/555-hw1/www");
//        HttpServer basicServer=new HttpServer(10,10,"/projects/555-hw1/www", true, s);
//        try {
//			basicServer.initializeHttpServer(9090);
//		} catch (InterruptedException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//        try {
//   			Thread.sleep(1000);
//   		} catch (InterruptedException e) {
//   			// TODO Auto-generated catch block
//   			e.printStackTrace();
//   		}
//      // HttpIoHandler.sendException(s, null, halt);
//        String result=new String("");
//        while(result.equals("")){
//        	result = byteArrayOutputStream.toString("UTF-8").replace("\r", "");
//        }
//        
//        System.out.println("The result is"+result);
// 
//        assertTrue(result.startsWith("HTTP/1.1 400"));
//
//        
//    }
//    @Test
//    public void testWrongDateFormat() throws IOException {
//    	final Logger logger = LogManager.getLogger(TestSendException.class);
//        logger.debug("***********************<testWrongDateFormat()>************************");
//        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
//        String S="GET /folder/../../src/main/java/edu/upenn/cis/cis455/HttpServiceController.java"+"\r\n";
//        String sampleGetRequest = 
//                "GET /" +"\r\n"+
//                "User-Agent: Mozilla/4.0 (compatible; MSIE5.01; Windows NT)\r\n" +
//                "Date: Tuesday, 26 September 2017 01:38:13 GMT"+"\r\n"+
//                "protocolVersion: HTTP/1.1"+"\r\n"+
//                "Host: localhost:8080\r\n" +
//                "Accept-Language: en-us\r\n" +
//                "Accept-Encoding: gzip, deflate\r\n" +
//                "Cookie: name1=value1;name2=value2;name3=value3\r\n" +
//                "Connection: Keep-Alive\r\n\r\n";
//        Socket s = TestHelper.getMockSocket(
//        		sampleGetRequest, 
//            byteArrayOutputStream);
//        
//        HaltException halt = new HaltException();
// 
//         staticFileLocation("/projects/555-hw1/www");
//        HttpServer basicServer=new HttpServer(10,10,"/projects/555-hw1/www", true, s);
//        try {
//			basicServer.initializeHttpServer(9001);
//		} catch (InterruptedException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//        try {
//   			Thread.sleep(1000);
//   		} catch (InterruptedException e) {
//   			// TODO Auto-generated catch block
//   			e.printStackTrace();
//   		}
//      // HttpIoHandler.sendException(s, null, halt);
//        String result=new String("");
//       // while(result.equals("")){
//        	result = byteArrayOutputStream.toString("UTF-8").replace("\r", "");
//       // }
//        
//        System.out.println("The result is"+result);
// 
//        assertTrue(result.startsWith("HTTP/1.1 400"));
//
//        
//    }
//   
//    @Test
//    public void testWrongprotocolversion() throws IOException {
//    	final Logger logger = LogManager.getLogger(TestSendException.class);
//        logger.debug("***********************<testWrongprotocolversion()>************************");
//        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
//        String S="GET /"+"\r\n";
//        String sampleGetRequest = 
//                "GET / HTTP/1.0" +"\r\n"+
//                "User-Agent: Mozilla/4.0 (compatible; MSIE5.01; Windows NT)\r\n" +
//                "Date: Tue, 26 Sep 2017 01:38:13 GMT"+"\r\n"+
//                "protocolVersion: HTTP/1.0"+"\r\n"+
//                "Host: localhost:9015\r\n" +
//                "Accept-Language: en-us\r\n" +
//                "Accept-Encoding: gzip, deflate\r\n" +
//                "Cookie: name1=value1;name2=value2;name3=value3\r\n" +
//                "Connection: Keep-Alive\r\n\r\n";
//        Socket s = TestHelper.getMockSocket(
//        		sampleGetRequest, 
//            byteArrayOutputStream);
//        
//        HaltException halt = new HaltException();
// 
//         staticFileLocation("/projects/555-hw1/www");
//        HttpServer basicServer=new HttpServer(10,10,"/projects/555-hw1/www", true, s);
//        try {
//			basicServer.initializeHttpServer(9015);
//		} catch (InterruptedException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//
//      // HttpIoHandler.sendException(s, null, halt);
//        String result=new String("");
//        while(result.equals("")){
//        	result = byteArrayOutputStream.toString("UTF-8").replace("\r", "");
//        }
//        
//        System.out.println("The result is"+result);
// 
//        assertTrue(result.startsWith("HTTP/1.1 505"));
//
//        
//    }
//    
//      @Test
//    public void testHead() throws IOException {
//    	final Logger logger = LogManager.getLogger(TestSendException.class);
//        logger.debug("***********************<testWrongprotocolversion()>************************");
//        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
//        
//        String sampleGetRequest = 
//                "HEAD / HTTP/1.1" +"\r\n"+
//                "User-Agent: Mozilla/4.0 (compatible; MSIE5.01; Windows NT)\r\n" +
//                "Date: Tue, 26 Sep 2017 01:38:13 GMT"+"\r\n"+
//                "protocolVersion: HTTP/1.0"+"\r\n"+
//                "Host: localhost:9080\r\n" +
//                "Accept-Language: en-us\r\n" +
//                "Accept-Encoding: gzip, deflate\r\n" +
//                "Cookie: name1=value1;name2=value2;name3=value3\r\n" +
//                "Connection: Keep-Alive\r\n\r\n";
//        Socket s = TestHelper.getMockSocket(
//        		sampleGetRequest, 
//            byteArrayOutputStream);
//        
//        HaltException halt = new HaltException();
// 
//         staticFileLocation("/projects/555-hw1/www");
//        HttpServer basicServer=new HttpServer(10,10,"/projects/555-hw1/www", true, s);
//        try {
//			basicServer.initializeHttpServer(9080);
//		} catch (InterruptedException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//
//      // HttpIoHandler.sendException(s, null, halt);
//        String result=new String("");
//        while(result.equals("")){
//        	result = byteArrayOutputStream.toString("UTF-8").replace("\r", "");
//        }
//        
//        System.out.println("The result is"+result);
// 
//        assertTrue(result.startsWith("HTTP/1.1 200")||result.startsWith("HTTP/1.1 404"));
//
//        
//    }
//    @Test
//    public void testIfModified() throws IOException {
//        	SimpleDateFormat sdf = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss z");
//			sdf.setTimeZone(TimeZone.getTimeZone("G"));
//			Date currentTime= new Date();
//    	final Logger logger = LogManager.getLogger(TestSendException.class);
//        logger.debug("***********************<testIfModified()>************************");
//        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
//        String S="GET /folder/../../src/main/java/edu/upenn/cis/cis455/HttpServiceController.java"+"\r\n";
//        String sampleGetRequest = 
//                "GET /" +"\r\n"+
//                "User-Agent: Mozilla/4.0 (compatible; MSIE5.01; Windows NT)\r\n" +
//                "Date: "+sdf.format(currentTime)+"\r\n"+
//                "protocolVersion: HTTP/1.1"+"\r\n"+
//                "If-Modified-Since: "+sdf.format(currentTime)+"\r\n"+
//                "Host: localhost:9019\r\n" +
//                "Accept-Language: en-us\r\n" +
//                "Accept-Encoding: gzip, deflate\r\n" +
//                "Cookie: name1=value1;name2=value2;name3=value3\r\n" +
//                "Connection: Keep-Alive\r\n\r\n";
//        Socket s = TestHelper.getMockSocket(
//        		sampleGetRequest, 
//            byteArrayOutputStream);
//        
//        HaltException halt = new HaltException();
// 
//         staticFileLocation("/projects/555-hw1/www");
//        HttpServer basicServer=new HttpServer(10,10,"/projects/555-hw1/www", true, s);
//        try {
//			basicServer.initializeHttpServer(9019);
//		} catch (InterruptedException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//        try {
//   			Thread.sleep(1000);
//   		} catch (InterruptedException e) {
//   			// TODO Auto-generated catch block
//   			e.printStackTrace();
//   		}
//      // HttpIoHandler.sendException(s, null, halt);
//        String result=new String("");
//        while(result.equals("")){
//        	result = byteArrayOutputStream.toString("UTF-8").replace("\r", "");
//        }
//        
//        System.out.println("The result is"+result);
// 
//        assertTrue(result.startsWith("HTTP/1.1 304"));
////assertTrue(true);
//        
//    }
//    @Test
//        public void testIfNotModified() throws IOException {
//        	SimpleDateFormat sdf = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss z");
//			sdf.setTimeZone(TimeZone.getTimeZone("G"));
//			Date currentTime= new Date();
//    	final Logger logger = LogManager.getLogger(TestSendException.class);
//        logger.debug("***********************<testIfNotModified()>************************");
//        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
//        String S="GET /folder/../../src/main/java/edu/upenn/cis/cis455/HttpServiceController.java"+"\r\n";
//        String sampleGetRequest = 
//                "GET /" +"\r\n"+
//                "User-Agent: Mozilla/4.0 (compatible; MSIE5.01; Windows NT)\r\n" +
//                "Date: "+sdf.format(currentTime)+"\r\n"+
//                "protocolVersion: HTTP/1.1"+"\r\n"+
//                "If-Unmodified-Since: Tue, 26 Sep 2017 01:38:13 GMT"+"\r\n"+
//                "Host: localhost:9099\r\n" +
//                "Accept-Language: en-us\r\n" +
//                "Accept-Encoding: gzip, deflate\r\n" +
//                "Cookie: name1=value1;name2=value2;name3=value3\r\n" +
//                "Connection: Keep-Alive\r\n\r\n";
//        Socket s = TestHelper.getMockSocket(
//        		sampleGetRequest, 
//            byteArrayOutputStream);
//        
//        HaltException halt = new HaltException();
// 
//         staticFileLocation("/projects/555-hw1/www");
//        HttpServer basicServer=new HttpServer(10,10,"/projects/555-hw1/www", true, s);
//        try {
//			basicServer.initializeHttpServer(9099);
//		} catch (InterruptedException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//        try {
//   			Thread.sleep(1000);
//   		} catch (InterruptedException e) {
//   			// TODO Auto-generated catch block
//   			e.printStackTrace();
//   		}
//      // HttpIoHandler.sendException(s, null, halt);
//        String result=new String("");
//        while(result.equals("")){
//        	result = byteArrayOutputStream.toString("UTF-8").replace("\r", "");
//        }
//        
//        System.out.println("The result is"+result);
// 
//        assertTrue(result.startsWith("HTTP/1.1 412")||result.startsWith("HTTP/1.1 404"));
////assertTrue(true);
//        
//    }
//    @Test
//    public void testAbsolutePath() throws IOException {
//    	final Logger logger = LogManager.getLogger(TestSendException.class);
//        logger.debug("***********************<testAbsolutePath()>************************");
//        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
//       
//        String sampleGetRequest = 
//                "GET http:/localhost:9011/..//src/main/java/edu/upenn/cis/cis455/m1/server/implementations/RequestImplementation.java" +"\r\n"+
//                "User-Agent: Mozilla/4.0 (compatible; MSIE5.01; Windows NT)\r\n" +
//                "Date: Tue, 26 Sep 2017 01:38:13 GMT"+"\r\n"+
//                "protocolVersion: HTTP/1.1"+"\r\n"+
//                "Host: localhost:9011\r\n" +                
//                "Accept-Language: en-us\r\n" +
//                "Accept-Encoding: gzip, deflate\r\n" +
//                "Cookie: name1=value1;name2=value2;name3=value3\r\n" +
//                "Connection: Keep-Alive\r\n\r\n";
//        Socket s = TestHelper.getMockSocket(
//        		sampleGetRequest, 
//            byteArrayOutputStream);
//        
// 
//         staticFileLocation("/projects/555-hw1/www");
//        HttpServer basicServer=new HttpServer(10,10,"/projects/555-hw1/www", true, s);
//        try {
//			basicServer.initializeHttpServer(9011);
//		} catch (InterruptedException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//        try {
//   			Thread.sleep(1000);
//   		} catch (InterruptedException e) {
//   			// TODO Auto-generated catch block
//   			e.printStackTrace();
//   		}
//      // HttpIoHandler.sendException(s, null, halt);
//        String result=new String("");
//        while(result.equals("")){
//        	result = byteArrayOutputStream.toString("UTF-8").replace("\r", "");
//        }
//        
//        System.out.println("The result is"+result);
// 
//        assertTrue(result.startsWith("HTTP/1.1 403")||result.startsWith("HTTP/1.1 404"));
//
//        
//    }
//    @Test
//    public void testAbsolutePath2() throws IOException {
//    	final Logger logger = LogManager.getLogger(TestSendException.class);
//        logger.debug("***********************<testAbsolutePath2()>************************");
//        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
//       
//        String sampleGetRequest = 
//                "GET http:/my.url/index.html" +"\r\n"+
//                "User-Agent: Mozilla/4.0 (compatible; MSIE5.01; Windows NT)\r\n" +
//                "Date: Tue, 26 Sep 2017 01:38:13 GMT"+"\r\n"+
//                "protocolVersion: HTTP/1.1"+"\r\n"+
//                "Host: my.url\r\n" +                
//                "Accept-Language: en-us\r\n" +
//                "Accept-Encoding: gzip, deflate\r\n" +
//                "Cookie: name1=value1;name2=value2;name3=value3\r\n" +
//                "Connection: Keep-Alive\r\n\r\n";
//        Socket s = TestHelper.getMockSocket(
//        		sampleGetRequest, 
//            byteArrayOutputStream);
//        
// 
//         staticFileLocation("/projects/555-hw1/www");
//        HttpServer basicServer=new HttpServer(10,10,"/projects/555-hw1/www", true, s);
//        try {
//			basicServer.initializeHttpServer(9012);
//		} catch (InterruptedException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//      
//        
//        
//        
//      // HttpIoHandler.sendException(s, null, halt);
//        String result=new String("");
//        while(result.equals("")){
//        	result = byteArrayOutputStream.toString("UTF-8").replace("\r", "");
//        }
//        
//        System.out.println("The result is"+result);
// 
//        assertTrue(result.startsWith("HTTP/1.1 200")||result.startsWith("HTTP/1.1 404"));
//
//        ;
//    }
//    
//    
    @After
    public void tearDown() {
    	
    	getSessionMap().clear();
    }
}
